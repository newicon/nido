var api = require('./api');
var helpers = require('./helpers');
var logger = require('./cmd/logger')();
var path = require('path');
var extend = require('util-extend');
var Fiber = require('fibers');
var _ = require('lodash');

/**
 * This is the base class for custom deployments. Extend Recipe class
 * to produce additional deployments for various project Types
 * 
 * @property {object} config becomes an object like:
 * { 
 *     releaseDir: '2015-09-06-110527',
 *     repo: 'git@bitbucket.org:newicon/newicon.net.git',
 *     recipe: 'wordpress',
 *     server: 'newicon.vm.newicon.net',
 *     domain: 'www.newicon.net',
 *     deployPath: '/srv/newicon.net',
 *     gitHead: 'f88a23c6fb8b6e9b7a62936e053dc525331976fc'
 * }
 * @property {string} environment the environment to deploy to, e.g. test, staging, production
 * @param {string} environment string the environment name
 * @returns {Recipe}
 */

var Recipe = {};
Recipe.config = {};

Recipe.setup = function(environment) {

	if (!environment) {
		api.abort('You must define an environment');
	}

	Recipe.environment = environment;

	/**
	 * { 
	 *     releaseDir: '2015-09-06-110527',
	 *     releasePath: '/srv/newicon.net/releases/2015-09-06-110527'
	 *     deployPath: '/srv/newicon.net' // if not specified it will be set to 
	 *     repo: 'git@bitbucket.org:newicon/newicon.net.git',
	 *     type: 'wordpress',
	 *     server: 'newicon.vm.newicon.net',
	 *     domain: 'www.newicon.net',
	 *     gitHead: 'f88a23c6fb8b6e9b7a62936e053dc525331976fc' (added by prepare method)
	 * }
	 */
	if (!Recipe.config.environments[environment]) {
		api.abort('Environment "' + environment + '" has not been configured');
	}
	Recipe.config.repo = Recipe.config.repo || api.findGitRepo();
	Recipe.config.releaseDir = Recipe.config.releaseDir || api.getLatestReleaseFolderName();
	extend(Recipe.config, api.getConfigFor(environment, Recipe));

	Recipe.local = api.getShell();
	Recipe.local.setTerms(Recipe.config);

	if (environment !== 'local') {
		// set up remote connection
		Recipe.remote = api.getSshToServer(Recipe.config.server);
		Recipe.remoteRoot = api.getSshRoot(Recipe.config.server);

		// pass a reference to the config object so our shell and ssh objects
		// automatically replace terms config keys in the command string
		Recipe.remote.setTerms(Recipe.config);
		Recipe.remoteRoot.setTerms(Recipe.config);
	} else {
		// if the environment is local, then the remote shell is the same as the local one
		Recipe.remote = Recipe.local;
	}

	return Recipe;
};


/**
 * Runs a tasks in a recipe
 * @see api.task()
 * @param taskName
 */
Recipe.run = function(taskName) {
	Recipe.taskLog(taskName);
	var toRun = Recipe.tasks[taskName];
	if (Array.isArray(toRun)) {
		// loop through and run each task sequentially
		toRun.forEach(function(task) {
			Recipe.run(task);
		}, Recipe);
	} else {
		if (!toRun) {
			logger.error('Unknown task "' + taskName + '"');
		}
		toRun.apply(Recipe, [Recipe.remote, Recipe.local, Recipe.remoteRoot]);
	}
};


/**
 * Runs the local build script
 * @returns {undefined}
 */
Recipe.preBuild = function(environment) {
	environment = environment || this.environment;
	var config = api.getConfigFor(environment, Recipe);
	var prebuild = true;
	if (typeof this.config.prebuild !== 'undefined' && !this.config.prebuild)
		prebuild = false;
	if (prebuild)
		Recipe.local.exec('./build');
	else
		Recipe.local.exec('echo # Build script not required / set');
};

Recipe.getRemote = function() {
	var remote = api.getSshToServer(Recipe.config.server);
	remote.setTerms(Recipe.config);
	return remote;
};

Recipe.getLocal = function() {
	var local = api.getShell();
	local.setTerms(Recipe.config);
	return local;
};

Recipe.tasks = {};
Recipe.taskDescriptions = {};

/**
 * Adds a task that can be run
 * @param taskName
 * @param arrayOrFunction
 * @param description
 */
Recipe.task = function(taskName, arrayOrFunction, description) {
	Recipe.tasks[taskName] = arrayOrFunction;
	Recipe.taskDescriptions[taskName] = description
};


Recipe.batch = function(description, env, fn, options) {
	Recipe.setup(env);
	options = options || {};
	Fiber(function() {
		api.ensureAuthorised();
		if (!options.silent)
			logger.info('Running: ' + description);
		var t = process.hrtime();
		fn();
		if (!options.silent)
			logger.info('Finished after ' + prettyTime(process.hrtime(t)));
		if (api._ssh) {
			api._ssh.close();
		}
	}).run();
};

/**
 * Alias of api getServerData
 * Allows the recipe to access the server information returned by the hub
 * @return {Object}
 */
Recipe.getServerData = function(server) {
	return api.getServerData(server);
};

/**
 * Alias of api getRootMysqlPassword
 * Allows the recipe to access the server information returned by the hub
 * @return {String}  the root mysql password
 */
Recipe.getRootMysqlPassword = function(server) {
	return api.getRootMysqlPassword(server);
};

Recipe.listTasks = function() {
	console.log(Recipe.tasks);
};

/**
 * TASK: shared - create symlinks to shared directories and files 
 * @see "sharedDirs" and "sharedFiles"
 * -----------------------------------------------------------------------------
 * Create shared directories, symlink project shared paths specified in "sharedDirs" to the shared directory
 * And symlink shared files in project specified by "sharedFiles" config option
 * @returns {undefined}
 */
Recipe.shared = function() {
	var $sharedPath = Recipe.config.deployPath + "/shared";
	Recipe.set('sharedPath', $sharedPath);
	Recipe.get('sharedDirs').forEach(function($dir) {
		Recipe.setSharedPathInfo($dir);
		Recipe.remote.log('Create shared directory: ' + Recipe.get('shared').projectDir + ' linking to ' + Recipe.get('shared').sharedDir);
		// Remove from source
		Recipe.remote.exec("if [ -d $(echo {releasePath}/{shared.projectDir}) ]; then rm -rf {releasePath}/{shared.projectDir}; fi");
		// Create shared dir if it does not exist
		Recipe.remote.exec("mkdir -p {sharedPath}/{shared.sharedDir}");
		// Create path to shared dir in release dir if it does not exist
		// (symlink will not create the path and will fail otherwise)
		Recipe.remote.exec("mkdir -p `dirname {releasePath}/{shared.projectDir}`");
		// Symlink shared dir to remotelease dir
		Recipe.remote.exec("ln -nfs {sharedPath}/{shared.sharedDir} {releasePath}/{shared.projectDir}");
		Recipe.remote.log('Complete: Create shared directory: ' + Recipe.get('shared').projectDir + ' linking to ' + Recipe.get('shared').sharedDir);
	}, Recipe);
	Recipe.get('sharedFiles').forEach(function($file) {
		Recipe.setSharedPathInfo($file);
		Recipe.remote.log('Create as shared file: ' + Recipe.get('shared').projectDir + ' linking to ' + Recipe.get('shared').sharedDir);
		// Remove from source
		Recipe.remote.exec("if [ -f $(echo {releasePath}/{shared.projectDir}) ]; then rm -rf {releasePath}/{shared.projectDir}; fi");
		// Create dir of shared file
		Recipe.remote.exec("mkdir -p {sharedPath}/".path.dirname(Recipe.get('shared.sharedDir')));
		// Touch shared
		Recipe.remote.exec("touch {sharedPath}/{shared.sharedDir}");
		// Symlink shared dir to release dir
		Recipe.remote.exec("ln -nfs {sharedPath}/{shared.sharedDir} {releasePath}/{shared.projectDir}");
		Recipe.remote.log('Complete: Create shared file: ' + Recipe.get('shared').projectDir + ' linking to ' + Recipe.get('shared').sharedDir);
	}, Recipe);
};

/**
 * TASK: writable - create writable directories 
 * @see "wirtableDirs"
 * -----------------------------------------------------------------------------
 * Make writable directories specifed in config option "writableDirs"
 * Make the uploads, runtime and assets folders writable. Set the group to www-data
 * For Recipe to work admin must be a member of www-data
 * on the server edit the /etc/group file
 * change the line similar to "www-data:x:503:" to "www-data:x:503:admin" (basically add admin on the end)
 * refer to wiki article https://hub.newicon.net/wiki-article/secure-deployment-permissions-for-web-accessible-uploads-and-media-folders
 * now admin has the ability to change files and folders to belong to the www-data group.
 * we want to trust www-data as little as possible as Recipe is what php runs as.  But admin we can trust more. 	
 * @return  {undefined}
 */
Recipe.writable = function() {
	Recipe.get('writableDirs').forEach(function($dir) {
		Recipe.set('dir', $dir);
		Recipe.setSharedPathInfo($dir, 'writable'); // creates specific {writable.projectDir} and {writable.sharedDir}
		Recipe.remote.log('Create writable directory: ' + Recipe.get('writable').projectDir);
		// create the directory if it does not exist
		Recipe.remote.exec('mkdir -p {releasePath}/{writable.projectDir}');
		includeHtaccess = ($dir.includeHtaccess === undefined) ? true : $dir.includeHtaccess;
		Recipe.remote.exec('chgrp www-data {releasePath}/{writable.projectDir} && chmod 1774 {releasePath}/{writable.projectDir}');

		// include the htaccess file
		if (includeHtaccess) {
			// content for htaccess file
			var htaccess = 'Options -Indexes\n'; // prevents people browsing the directory (if web accessible)
			htaccess += 'RemoveHandler .php .phtml .php3 .php4 .php5 .pl .py .jsp .asp .htm .shtml .sh .cgi\n';
			htaccess += 'RemoveType .php .phtml .php3 .php4 .php5 .pl .py .jsp .asp .htm .shtml .sh .cgi\n';
			htaccess += 'php_flag engine off';
			var exists = Recipe.remote.exec('if [ -f {releasePath}/{writable.projectDir}/.htaccess ]; then echo "true"; else echo "false"; fi');
			if (!_.isString(exists.stdout)) {
				console.log('Failed to determine if .htaccess exists ... it will be re-created');
				console.error(exists);
				exists.stdout='false';
			}

			// we remove the permissions after creation so if the htaccess file exists
			// we don't want to try and recreate it.
			
			if (_.isString(exists.stdout) && exists.stdout.trim() === 'false') {
				Recipe.remote.exec('rm -rf {releasePath}/{writable.projectDir}/.htaccess');
				// create the htaccess file and remove permissions
				Recipe.remote.exec('echo "' + htaccess + '" > {releasePath}/{writable.projectDir}/.htaccess && chmod 0444 {releasePath}/{writable.projectDir}/.htaccess');
			}
		}
	}, Recipe);
	Recipe.get('writableFiles').forEach(function($file) {
		Recipe.set('file', $file);
		Recipe.remote.log('Create writable file: ' + Recipe.get('file'));
		Recipe.remote.exec('chgrp www-data {releasePath}/{file} && chmod 1774 {releasePath}/{file}');
	});
	Recipe.remote.log('Completed: Create writable directories');
};

/**
 * TASK: Main deplyments process: check out git and copy to release folder
 * -----------------------------------------------------------------------------
 * get the latest git, pop it in repo.  Then copy accross to the new release folder
 * @returns {undefined}
 */
Recipe.release = function() {
	Recipe.remote.exec('if [ -d "{deployPath}/shared/repo" ]; then cd {deployPath}/shared/repo && git fetch -q origin && git fetch --tags -q origin && git reset -q --hard {gitHead} && git clean -q -d -x -f; else git clone -q {repo} {deployPath}/shared/repo && cd {deployPath}/shared/repo && git checkout -q -b deploy {gitHead}; fi');
	// copy the repository into the release folder
	Recipe.remote.exec('cp -RPp {deployPath}/shared/repo {deployPath}/releases/{releaseDir}');
	// place a file in the new release folder with the git revision (gitHead) id in it
	Recipe.remote.exec('echo {gitHead} > {deployPath}/releases/{releaseDir}/REVISION');
};

/**
 * TASK: DEPLOY !! symlink Deploy!
 * -----------------------------------------------------------------------------
 * @returns {undefined}
 */
Recipe.symlink = function() {
	// This line effectively deploys the code as it switches the current release folder to the latest (newly deployed release folder).
	Recipe.remote.exec('ln -nfs {deployPath}/releases/{releaseDir} {deployPath}/current');
	Recipe.remote.exec('if [ -d $(echo {deployPath}/public/htdocs) ]; then rm -rf {deployPath}/public/htdocs; fi')
	if (!Recipe.get('webRoot') || Recipe.get('webRoot') == '') {
		Recipe.remote.exec('ln -nfs {deployPath}/current {deployPath}/public/htdocs');
	} else {
		Recipe.remote.exec('ln -nfs {deployPath}/current/{webRoot} {deployPath}/public/htdocs');
	}
};

/**
 * TASK: config - generate the project configuration file 
 * -----------------------------------------------------------------------------
 * @see "configFile"
 * Generate the wp-config file with the db credentials for wordpress
 * @returns {undefined}
 */
Recipe.genConfig = function() {
	var configFile = Recipe.getConfigFileContents();
	Recipe.remote.exec('echo \'' + configFile + '\' > {releasePath}/{configFile}', {
		silent: true
	});
};

/**
 * Generate the config file contents
 * @return {string}
 */
Recipe.getConfigFileContents = function() {
	Recipe.remote.abort('function: "getConfigFileContents" is not implemented by the recipe "' + Recipe.config.recipe + '"');
};

/**
 * Rollback the last deployment
 * @returns {undefined}
 */
Recipe.rollback = function() {
	Recipe.remote.abort('Not implemented by the recipe "' + Recipe.config.recipe + '"');
};

/**
 * ==============================================================================================================
 * ::: nido pullmedia environment ::: Pull media files into local
 * ==============================================================================================================
 * Runs an rsync command to pull shared into local folder, @see Recipe.set('media')
 * @return {undefined}
 */
Recipe.pullMedia = function() {
	var media = Recipe.get('media');
	// if the media option is empty then assume we want to sync with all the shared Dirs
	if (media.length == 0) {
		media = Recipe.get('sharedDirs');
	}
	media.forEach(function($dir) {
		Recipe.setSharedPathInfo($dir);
		Recipe.set('local', api.getConfigFor('local', Recipe));
		var get = Recipe.get;
		Recipe.local.log('Pull media from ' + get('deployPath') + '/shared/' + get('shared').sharedDir + ' on the ' + Recipe.environment + ' environment into ' + get('local').deployPath + '/' + get('shared').projectDir);
		// if we are syncing wp-content/uploads in shared/uploads
		Recipe.local.exec('rsync --progress --compress --rsh=/usr/bin/ssh --recursive admin@{server}:{deployPath}/shared/{shared.sharedDir}/ {local.deployPath}/{shared.projectDir}');
	}, Recipe);
};

/**
 * ==============================================================================================================
 * ::: nido pushmedia environment ::: Push media files from local to environment
 * ==============================================================================================================
 * Runs an rsync command to push local media to the remote media @see Recipe.set('media')
 * @return void
 */
Recipe.pushMedia = function(changeToWWWData) {
	if (typeof changeToWWWData==='undefined') {
		changeToWWWData = true;
	}
	var media = Recipe.get('media');
	// if the media option is empty then assume we want to sync with all the shared Dirs
	if (media.length == 0) {
		media = Recipe.get('sharedDirs');
	}
	media.forEach(function($dir) {
		Recipe.setSharedPathInfo($dir);
		Recipe.set('local', api.getConfigFor('local', Recipe));
		var get = Recipe.get;
		Recipe.local.log('Push media from ' + get('local').deployPath + '/' + get('shared').projectDir) + ' to ' + get('deployPath') + '/shared/' + get('shared').sharedDir + ' on the ' + Recipe.environment + ' environment';
		// the rsync command needs to be /wp-content/uploads /shared , no uploads folder on the end as it will be created
		Recipe.local.exec('rsync --progress --compress --rsh=/usr/bin/ssh --recursive --exclude \'catalog/product/cache\' {local.deployPath}/{shared.projectDir}/ admin@{server}:{deployPath}/shared/{shared.sharedDir}');
		// need a solution to ensure sub folders are owned by www-data or have appropriate permissions set. – done by Nicole 23/6/17
	}, Recipe);
	if (changeToWWWData) {
		Recipe.remote.log("Changing to www-data");
		Recipe.remote.exec('chown -R admin:www-data {deployPath}/shared/.');
	}
};

/**
 * Sets a 'shared' property on the config making 
 * {shared.sharedDir} and {shared.projectDir} available
 * @param mixed $dir either a string file path 
 * like: 'path/to/dir' or an object like:
 * {'uploads':'wp-content/uploads'}
 * @param {string} nameSpace the key in the config to store this under, defaults to 'shared'
 * @returns {undefined}
 */
Recipe.setSharedPathInfo = function($dir, nameSpace) {
	nameSpace = nameSpace || 'shared';
	var isObject = (Object.prototype.toString.call($dir) === '[object Object]');
	Recipe.set(nameSpace, {
		sharedDir: isObject ? Object.keys($dir)[0] : $dir,
		projectDir: isObject ? $dir[Object.keys($dir)[0]] : $dir
	});
};

/**
 * Called via nido pushdb environment
 *
 * ```nido pushdb test``` 
 *
 * Takes your current local database. Formats the url's to correct for the target environment
 * and then imports the database on the target environment.  Can be very usefull to push local databse changes to a
 * remote test server. etc.
 *
 * @return void
 */
Recipe.pushDb = function() {
	// prompt when deploying to a specific target
	if (Recipe.environment === 'production') {
		var input = Recipe.local.prompt('You are about to push your local database to production, are you sure you want to do this? [yes]');
		if (input.indexOf('yes') === -1) {
			Recipe.local.abort('Cancelled, phew I thought you were about to completely �!%! everything');
		}
	}

	var config = api.getConfigFor(Recipe.environment, Recipe);
	var dbObjKeys = Object.keys(config.db);
	if (dbObjKeys.length === 0) {
		throw "No database configuration set."
	} else if (dbObjKeys.indexOf('database') > -1 && dbObjKeys.indexOf('username') > -1 && dbObjKeys.indexOf('password') > -1) {
		Recipe.local.log('old db push');
		Recipe.mysqlLocalDump('deploy/local-dump.sql');
		Recipe.mysqlImportIntoRemote('deploy/local-dump.sql');
	} else {
		dbObjKeys.forEach(function (e) {
			if (e === 'host') return;
			var subConfig = config.db[e];
			var subDbObjKeys = Object.keys(subConfig);
			if (subDbObjKeys.indexOf('database') > -1 && subDbObjKeys.indexOf('username') > -1 && subDbObjKeys.indexOf('password') > -1) {
				Recipe.mysqlLocalDump('deploy/local-dump-'+e+'.sql', e);
				Recipe.mysqlImportIntoRemote('deploy/local-dump-'+e+'.sql', e);
			} else {
				throw "Could not find database config for " + e +". Make sure there is a database, username and password entry.";
			}
		});
	}
};



/**
 * ==============================================================================================================
 * ::: nido pulldb environment ::: Pull the databsase and media from an environment.  Set up local
 * ==============================================================================================================
 * Called via nido pulldb environment
 * ```nido pulldb environment```
 * will pull the database from the specified environment into the local environment
 * correct the urls and import into the local environment
 * @return void
 */
Recipe.pullDb = function() {
	Recipe.mysqlRemoteDump('deploy/remote-dump.sql');
	Recipe.mysqlImportIntoLocal('deploy/remote-dump.sql');
};

/**
 * Generates a remote-dump of the database, and puts the file in the current working directory,
 * excluding tables in globalExcludedTables and remote db.excludePull
 * @param {string|null} dbId  indicate which database to use from the config.
 * 		(only used if more than one database needed for the deployment)
 * 		e.g. if config looks like this
 * 			local: {
 *				env:'dev',
 *				...
 *				db: {
 *					prestashop: {
 *						database: 'prestashop_db',
 *						username: 'prestashop_user',
 *						password: 'prestashop_password'
 *					},
 *					wordpress: {
 *						database: 'wordpress_db',
 *						username: 'wordpress_user',
 *						password: 'wordpress_password'
 *					}
 *				},
 *				...
 *			},
 *		then you need to set the dbId to either 'prestashop' or 'wordpress'.
 * @returns {undefined}
 */
Recipe.mysqlRemoteDump = function(dumpFile, dbId = null) {
	Recipe.set('local', api.getConfigFor('local', Recipe));
	Recipe.set('dumpFile', dumpFile || 'remote-dump.sql');
	// create the path to where the dumpFile should be created
	Recipe.local.exec('mkdir -p ' + path.dirname(dumpFile));
	// dump the remote db into dumpFile
	// List tables command and trim header
	Recipe.local.log('Listing all tables on the source database');
	var dbConfigPath = (dbId === null) ? 'db' : 'db.'+dbId;
	var listTablesCmd = "ssh admin@{server} 'mysql -hlocalhost -u{"+dbConfigPath+".username} -p{"+dbConfigPath+".password} {"+dbConfigPath+".database} -e" +
		" \"show tables;\" | sed -n -e '1!p' '";
	var execResult = Recipe.local.exec(listTablesCmd, {silent: true});
	var allTables = [];
	if (execResult.stdout) {
		allTables = execResult.stdout;
		allTables = allTables.split('\n');
		Recipe.local.log('All tables in source database: \n' + allTables.join(' '));
	}
	else {
		Recipe.local.log('Failed to retrieve table list from the source database, wildcards in the include/exclude' +
			' list will not work.');
	}
	Recipe.set('allTables', allTables);
	var combinedExcludedTables = [], excludeList = [], includeList = [];
	if (this.config.globalExcludedTables != null && this.config.globalExcludedTables.length >= 1) {
		combinedExcludedTables = helpers.expandListWithMatchingTables(this.config.globalExcludedTables, allTables);
		Recipe.local.log('Globally excluded tables expanded: \n' + combinedExcludedTables.join(' '));
	}

	var remoteConfig = api.getConfigFor('remote', Recipe); // JSON of remote config
	var remoteDbConfig = (dbId === null) ? remoteConfig.db : remoteConfig.db[dbId];
	if (remoteDbConfig.excludePull != undefined && remoteDbConfig.excludePull != null && remoteDbConfig.excludePull.length >= 1) {
		excludeList = helpers.expandListWithMatchingTables(remoteDbConfig.excludePull, allTables);
		Recipe.local.log('excludePull tables expanded: \n' + excludeList.join(' '));
	}
	if (remoteDbConfig.includePull != undefined && remoteDbConfig.includePull != null && remoteDbConfig.includePull.length >= 1) {
		includeList = helpers.expandListWithMatchingTables(remoteDbConfig.includePull, allTables);
		Recipe.local.log('includePull tables expanded: \n' + includeList.join(' '));
	}

	// Concat with the global exclude list, preserving only unique entries
	combinedExcludedTables = combinedExcludedTables.concat(excludeList.filter(function(val) {
		return (combinedExcludedTables.indexOf(val) == -1); //only insert table names not already in the list
		// i.e. unique
	}));
	// Local include list is the superconfig which will overrule anything given in global exclude and local exclude
	combinedExcludedTables = _.uniq(_.difference(combinedExcludedTables, includeList)); // subtract local include list from
	// global+local exclude list

	Recipe.local.log('Resulting list of excluded tables: \n' + combinedExcludedTables.join(' '));
	var tablesToPull = _.uniq(_.difference(Recipe.get('allTables'), combinedExcludedTables));
	Recipe.local.log('List of tables to pull: \n' + tablesToPull.join(' '));
	var excludeTablesStr= "";
	// construct the ignore tables string from the array of unique table names
	if (combinedExcludedTables.length >= 1) {
		var dbName = remoteDbConfig.database;
		excludeTablesStr = combinedExcludedTables.reduce(function(acc, val) {
			acc += " --ignore-table=" + dbName + "." + val;
			return acc;
		}, "");
	}

	var optionalDumpPipe = ('pulldbMysqldumpPipe' in Recipe.config)
		? " | "+Recipe.config.pulldbMysqldumpPipe
		: "";

	// --single-transaction is the default to ensure back compatibility
	var optionalDumpParms = ('mysqldumpParams' in Recipe.config)
		? " "+Recipe.config.mysqldumpParams
		: " --single-transaction";

	var dumpCmd = "ssh admin@{server} 'mysqldump -h 127.0.0.1 -u{db.username} -p{db.password}"+optionalDumpParms+" {db.database}"+ excludeTablesStr + "'"
		+ optionalDumpPipe
		+ " > {dumpFile}";

	Recipe.local.exec(dumpCmd, {
		silent: true
	});
};

/**
 * import a remote dump mysql file into the local environment
 * @param {string|null} dbId  indicate which database to use from the config.
 * 		(only used if more than one database needed for the deployment)
 * 		e.g. if config looks like this
 * 			local: {
 *				env:'dev',
 *				...
 *				db: {
 *					prestashop: {
 *						database: 'prestashop_db',
 *						username: 'prestashop_user',
 *						password: 'prestashop_password'
 *					},
 *					wordpress: {
 *						database: 'wordpress_db',
 *						username: 'wordpress_user',
 *						password: 'wordpress_password'
 *					}
 *				},
 *				...
 *			},
 *		then you need to set the dbId to either 'prestashop' or 'wordpress'.
 * @returns {undefined}
 */
Recipe.mysqlImportIntoLocal = function(dumpFile, dbId = null) {
	var terms = Recipe.config;
	terms.local = api.getConfigFor('local', Recipe);
	terms.dumpFile = dumpFile || 'local-dump.sql';

	// process the dump file before importing
	Recipe.mysqlProcessDumpFile(dumpFile, 'local', undefined, dbId);

	var localDbConfigPath = (dbId === null) ? "local.db" : "local.db."+dbId;
	var localDbConfig = (dbId === null) ? terms.local.db : terms.local.db[dbId];

	// get the sql user from the local file if specified or otherwise assume root
	var userSql = (localDbConfig.password != undefined && localDbConfig.password != '') ? ' -u {'+localDbConfigPath+'.username}' : ' -u root';
	// if localhost password is empty then remove -p from command to prevent the "Enter Password:" prompt
	var passwordSql = (localDbConfig.password != undefined && localDbConfig.password != '') ? ' -p{'+localDbConfigPath+'.password}' : '';
	// create local backup incase import fails
	Recipe.local.log('Create local database backup dump file in case importing fails');
	Recipe.local.exec('mkdir -p deploy');

	var optionalDumpParms = ('mysqldumpParams' in Recipe.config)
		? " "+Recipe.config.mysqldumpParams
		: "";
	Recipe.local.exec("mysqldump -h 127.0.0.1 " + userSql + passwordSql + optionalDumpParms+" {local.db.database} > deploy/{local.db.database}_bkup", {terms: terms});
	Recipe.local.exec("mysql -h 127.0.0.1 " + userSql + passwordSql + " -e 'DROP DATABASE {local.db.database}'", {terms: terms});
	Recipe.local.exec("mysql -h 127.0.0.1 " + userSql + passwordSql + " -e 'CREATE DATABASE IF NOT EXISTS {local.db.database}'", {terms: terms});

	// attempt to create the local database if it does not exist
	Recipe.local.exec("mysql -h 127.0.0.1 " + userSql + passwordSql + " -e 'CREATE DATABASE IF NOT EXISTS {"+localDbConfigPath+".database}'", {terms: terms});
	Recipe.local.exec('mysql -h 127.0.0.1 ' + userSql + passwordSql + ' {'+localDbConfigPath+'.database} < {dumpFile}', {terms: terms});
};

/**
 * Makes a dump of the local database, excluding tables in globalExcludedTables and remote db.excludePush
 * @param {type} dumpFile
 * @param {string|null} dbId  indicate which database to use from the config.
 * 		(only used if more than one database needed for the deployment)
 * 		e.g. if config looks like this
 * 			local: {
 *				env:'dev',
 *				...
 *				db: {
 *					prestashop: {
 *						database: 'prestashop_db',
 *						username: 'prestashop_user',
 *						password: 'prestashop_password'
 *					},
 *					wordpress: {
 *						database: 'wordpress_db',
 *						username: 'wordpress_user',
 *						password: 'wordpress_password'
 *					}
 *				},
 *				...
 *			},
 *		then you need to set the dbId to either 'prestashop' or 'wordpress'.
 * @returns {undefined}
 */
Recipe.mysqlLocalDump = function(dumpFile, dbId = null) {
	var terms = Recipe.config;
	Recipe.set('local', api.getConfigFor('local', Recipe));
	Recipe.set('dumpFile', dumpFile || 'local-dump.sql');
	// create the path to where the dumpFile should be created
	Recipe.local.exec('mkdir -p ' + path.dirname(dumpFile));

	var localDbConfigPath = (dbId === null) ? "local.db" : "local.db."+dbId;
	var localDbConfig = (dbId === null) ? terms.local.db : terms.local.db[dbId];
	// get the sql user from the local file if specified or otherwise assume root
	var userSql = (localDbConfig.password != undefined && localDbConfig.password != '') ? ' -u {'+localDbConfigPath+'.username}' : ' -u root';
	// if localhost password is empty then remove -p from command to prevent the "Enter Password:" prompt
	var passwordSql = (localDbConfig.password != undefined && localDbConfig.password != '') ? ' -p{'+localDbConfigPath+'.password}' : '';
	var remoteConfig = api.getConfigFor('remote', Recipe); // JSON of remote config
	// List tables command and trim header
	Recipe.local.log('Listing all tables on the source database');
	Recipe.local.log('dbId = '+dbId);
	var listTablesCmd = "mysql -h localhost " + userSql + passwordSql + " {"+localDbConfigPath+".database} -e" +
		" \"show tables;\" | sed -n -e '1!p'";
	var execResult = Recipe.local.exec(listTablesCmd, {silent: true});
	var allTables = [];
	if (execResult.stdout) {
		allTables = execResult.stdout;
		allTables = allTables.split('\n');
		Recipe.local.log('All tables in source database: \n' + allTables.join(' '));
	}
	else {
		Recipe.local.log('Failed to retrieve table list from the source database, wildcards in the include/exclude' +
			' list' +
			' will not work.');
	}
	Recipe.set('allTables', allTables);
	var combinedExcludedTables = [], excludeList = [], includeList = [];
	if (this.config.globalExcludedTables != null && this.config.globalExcludedTables.length >= 1) {
		combinedExcludedTables = helpers.expandListWithMatchingTables(this.config.globalExcludedTables, allTables);
		Recipe.local.log('Globally excluded tables expanded: \n' + combinedExcludedTables.join(' '));
	}
	var remoteDbConfig = (dbId === null) ? remoteConfig.db : remoteConfig.db[dbId];
	// look under excludePush of the REMOTE to get the list of tables to exclude from the local dump -> remote
	if (remoteDbConfig.excludePush != null && remoteDbConfig.excludePush.length >= 1) {
		excludeList = helpers.expandListWithMatchingTables(remoteDbConfig.excludePush, allTables);
		Recipe.local.log('excludePush tables expanded: \n' + excludeList.join(' '));
	}
	// look under includePush of the REMOTE to get the list of tables to include from the local dump -> remote
	if (remoteDbConfig.includePush != null && remoteDbConfig.includePush.length >= 1) {
		includeList = helpers.expandListWithMatchingTables(remoteDbConfig.includePush, allTables);
		Recipe.local.log('includePush tables expanded: \n' + includeList.join(' '));
	}

	// Concat with the global exclude list, preserving only unique entries
	combinedExcludedTables = combinedExcludedTables.concat(excludeList.filter(function(val) {
		return (combinedExcludedTables.indexOf(val) == -1); //only insert table names not already in the list
		// i.e. unique
	}));
	// Local include list is the superconfig which will overrule anything given in global exclude and local exclude
	combinedExcludedTables = _.uniq(_.difference(combinedExcludedTables, includeList)); // subtract local include list from
	// global+local exclude list

	Recipe.local.log('Resulting list of excluded tables: \n' + combinedExcludedTables.join(' '));
	var tablesToPull = _.uniq(_.difference(Recipe.get('allTables'), combinedExcludedTables));
	Recipe.local.log('List of tables to push: \n' + tablesToPull.join(' '));
	var excludeTablesStr= "";
	// construct the ignore tables string from the array of unique table names
	if (combinedExcludedTables.length >=1) {
		var dbName =  localDbConfig.database;
		excludeTablesStr = combinedExcludedTables.reduce(function(acc, val) {
			acc += " --ignore-table=" + dbName + "." + val;
			return acc;
		}, "");
	}

	var optionalDumpPipe = ('pushdbMysqldumpPipe' in Recipe.config)
		? " | "+Recipe.config.pushdbMysqldumpPipe
		: "";
	var optionalDumpParms = ('mysqldumpParams' in Recipe.config)
		? " "+Recipe.config.mysqldumpParams
		: "";
	var dumpCmd = 'mysqldump -h 127.0.0.1 ' + userSql + passwordSql + optionalDumpParms+' {local.db.database}' + excludeTablesStr
		+ optionalDumpPipe
		+ ' > {dumpFile}';
	
	Recipe.local.exec(dumpCmd);
};

/**
 * 
 * @param {type} dumpFile
 * @param {string|null} dbId  indicate which database to use from the config.
 * 		(only used if more than one database needed for the deployment)
 * 		e.g. if config looks like this
 * 			local: {
 *				env:'dev',
 *				...
 *				db: {
 *					prestashop: {
 *						database: 'prestashop_db',
 *						username: 'prestashop_user',
 *						password: 'prestashop_password'
 *					},
 *					wordpress: {
 *						database: 'wordpress_db',
 *						username: 'wordpress_user',
 *						password: 'wordpress_password'
 *					}
 *				},
 *				...
 *			},
 *		then you need to set the dbId to either 'prestashop' or 'wordpress'.
 * @returns {undefined}
 */
Recipe.mysqlImportIntoRemote = function(dumpFile, dbId = null) {

	// first backup the remote database
	var backUpFile = path.dirname(dumpFile) + '/pushdb-remote-backup.sql';
	Recipe.local.log('Backing up the remote database and storing locally in ' + backUpFile);
	Recipe.mysqlRemoteDump(backUpFile, dbId);
	// do the import
	Recipe.set('dumpFile', dumpFile || 'local-dump.sql');
	Recipe.mysqlProcessDumpFile(dumpFile, 'remote', undefined, dbId);
	// push to the remote
	var remoteDbConfigPath = 'db';
	if (dbId !== null) {
		remoteDbConfigPath = 'db.' + dbId;
	}
	Recipe.local.exec("ssh admin@{server} 'mysql -hlocalhost -u{"+remoteDbConfigPath+".username} -p{"+remoteDbConfigPath+".password} {"+remoteDbConfigPath+".database}' < {dumpFile}");
};

/**
 * 
 * @param {type} dumpFile
 * @param {type} prepareFor
 * @param {Function} function to call when dump file is successfully processed - passed the file path of the processed dump file
 * @param {string|null} dbId  indicate which database to use from the config.
 * 		(only used if more than one database needed for the deployment)
 * 		e.g. if config looks like this
 * 			local: {
 *				env:'dev',
 *				...
 *				db: {
 *					prestashop: {
 *						database: 'prestashop_db',
 *						username: 'prestashop_user',
 *						password: 'prestashop_password'
 *					},
 *					wordpress: {
 *						database: 'wordpress_db',
 *						username: 'wordpress_user',
 *						password: 'wordpress_password'
 *					}
 *				},
 *				...
 *			},
 *		then you need to set the dbId to either 'prestashop' or 'wordpress'.
 * @returns {undefined}
 */
Recipe.mysqlProcessDumpFile = function(dumpFile, prepareFor, callback, dbId = null) {};

/**
 * Output the releases (and highlight the current release) to console
 * @returns {undefined}
 */
Recipe.showReleases = function() {
	var releasesArray = Recipe.releasesArray();
	// get current release
	var current = Recipe.getCurrentReleaseDetails().currentDir;
	logger.log("\n");
	logger.success('Current releases:\n');
	for (var i in releasesArray) {
		if (releasesArray[i] === current) {
			logger.info(releasesArray[i] + '    (current)');
		} else {
			if (releasesArray[i] != '') {
				logger.log('- ' + releasesArray[i]);
			}
		}
	}
	logger.log("\n");
};

/**
 * Output to console the config for the current environment
 * @returns {undefined}
 */
Recipe.showConfig = function() {
	logger.log("\n");
	logger.success('The config for environment ' + Recipe.environment + ':');
	logger.log("\n");
	console.log(Recipe.config);
	logger.log("\n");
};

/**
 * Output the path to the current folder and the current folder name
 * @returns {undefined}
 */
Recipe.showCurrentRelease = function() {
	var currentDetails = Recipe.getCurrentReleaseDetails();
	logger.log("\n");
	logger.success('The current release for ' + Recipe.environment + ' is on ' + Recipe.config.server + ' at:');
	logger.log("\n");
	logger.log(currentDetails.currentPath);
	logger.info(currentDetails.currentDir);
	logger.log("\n");
};

/**
 * Gets details about the current deployed symlink directory
 * @return object with keys {currentPath: 'path/to/current/release/2015-01-01', currentDir: '2015-01-01'}
 */
Recipe.getCurrentReleaseDetails = function() {
	if (!Recipe.remote.directoryExists('{deployPath}/current', Recipe.config)) {
		Recipe.remote.abort('No current release folder exists');
	}
	var currentPath = Recipe.remote.exec('readlink {deployPath}/current', {
		terms: Recipe.config,
		failsafe: true
	}).stdout;

	// for some reason the readlink command above sometimes returns null;
	// test to make sure the string operations do not fail
	var currentPathResult = '';
	var currentDirResult = '';
	if (typeof currentPath === 'string') {
		currentPathResult = currentPath.trim();
		currentDirResult = path.basename(currentPath);
	}
	return {
		'currentPath': currentPathResult,
		'currentDir': currentDirResult
	};
};

/**
 * Rollback a deployment to a specific release.
 * @param {string} releaseDir the string directory name of the release dir to roll back to
 */
Recipe.rollbackTo = function(releaseDir) {
	// Generates: ln -nfs /srv/newiconnet.test.newicon.net/releases/2015-09-23-165942 /srv/newiconnet.test.newicon.net/current
	Recipe.set('rollbackReleaseDir', releaseDir);
	Recipe.remote.exec('ln -nfs {deployPath}/releases/{rollbackReleaseDir} {deployPath}/current')
}

/**
 * Set a config key name and value
 */
Recipe.set = function(name, value) {
	Recipe.config[name] = value;
};

/**
 * Get a config key name and value
 */
Recipe.get = function(name, value) {
	return Recipe.config[name];
};

/**
 * Get and array of release directories currently on the specified environment
 * @returns {Array}
 */
Recipe.releasesArray = function() {
	return Recipe.remote.exec('ls -1r {deployPath}/releases', {
		terms: Recipe.config,
		silent: true
	}).stdout.split("\n");
};

/**
 * Log when a task has started
 */
Recipe.taskLog = function(taskName) {
	Recipe.remote.log('==========================================================');
	Recipe.remote.log('Task: ' + taskName);
	if (Recipe.taskDescriptions[taskName]) {
		Recipe.remote.log('----------------------------------------------------------');
		Recipe.remote.log(Recipe.taskDescriptions[taskName]);
	}
};

Recipe.finish = function() {
	console.log('Finished');
};

module.exports = Recipe;
