var pkg = require('../package.json');
var program = require('commander');
var Fiber = require('fibers');
var Shell = require('../cmd/shell');
var Ssh = require('../cmd/ssh');
var errors = require('../cmd/errors');
var logger = require('../cmd/logger')();
var api = require('../api');
var helpers = require('../helpers');
var prettyTime = require('pretty-hrtime');
var extend = require('util-extend');
var inquirer = require('inquirer');
var fs = require('fs');
const path = require('path');
var Configstore = require('configstore');

// catch errors and exit
process.on('SIGINT', function() {
	throw new errors.ProcessInterruptedError(pkg.name + ' was interrupted');
});
process.on('uncaughtException', function (err) {
	var message = err.stack;
	if (err instanceof errors.BaseError) {
		message = err.message;
	}
	console.log(err);
	logger.error(message);
	process.exit(1);
});

// check for available updates:
var updateNotifier = require('update-notifier');
var notifier = updateNotifier({pkg: pkg, updateCheckInterval:1});
if (notifier.update) {
	notifier.notify();
}

program
	.option('-d, --dry-run','output commands to console rather than executing them');

// list commands:
program
	.version(pkg.version)
	.usage('[command|options]');
	
program
	.command('login')
	.description('Login and authorise with the hub')
	.action(function(option, value) {
		api.login();
	});
	
program
	.command('logout')
	.description('Logout and remove authorisation with the hub')
	.action(function(option, value) {
		api.logout();
	});


program.command('init')
	.description('Create a new nido.js file in the root of a project you wish to deploy')
	.action(function(option, value) {
		var prompts = [
			{
				type: 'list', name: 'recipe', message: 'Select a recipie?', choices: function() {
					var files = fs.readdirSync(__dirname+'/../recipes');
					let recipes = [];
					for (var i in files) {
						recipes.push( files[i].substring(0,  files[i].length-3)); 
					}
					return recipes;
				}
			}
		];

		if (fs.existsSync(process.cwd() + '/nido.js')) {
			prompts.push({type:'confirm', name:'overwrite', default:null, message: 'Nido.js file already exists, would you like to overwrite it?'})
		}

		inquirer
		.prompt(prompts)
		.then(answers => {
			let nidojs = `
module.exports = function(nido, env) {

	nido = nido('${answers.recipe}');
	
	nido.set('environments', {

		// local development environment
		local: {
			env:'dev',
			debug:true,
			domain:'yourdomain_development',
			deployPath: '~/Sites/yourdomain',
			db: {
				database: 'yourdomain_development',
				username: 'root',
				password: ''
			},
			gitBranch: "develop",
			siteName: "Your domain",
		},

		// test environment if applicable
		test: {
			env:'test',
			debug:true,
			domain:'TEST_WEBSITE',
			server: 'TEST_SERVER',
			db: {
				database: 'NAMESPACE_test',
				username: 'TEST_USER',
				password: 'TEST_PASSWORD'
			},
			gitBranch: "staging",
			siteName: 'SITE TITLE - Test',
		},

		// production environment
		staging: {
			env: 'prod',
			debug: false,
			domain:'WEBSITE',
			server: 'STAGING_SERVER',
			db: {
				database: 'NAMESPACE_staging',
				username: 'STAGING_USER',
				password: 'STAGING WEBSITE'
			},
			gitBranch: "master",
			siteName: 'SITE TITLE',
		},

		// production environment
		production: {
			env: 'prod',
			debug: false,
			domain:'WEBSITE',
			server: 'PRODUCTION_SERVER',
			db: {
				database: 'NAMESPACE_production',
				username: 'PRODUCTION_USER',
				password: 'PRODUCTION WEBSITE'
			},
			gitBranch: "master",
			siteName: 'SITE TITLE',
		},
	});

	return nido;
}
`;			
			// exit if we do not wish to overwrite the existing nido.js file
			if (answers.overwrite === false) return;

			fs.writeFile('nido.js', nidojs, 'utf8', (err) => {
				if (err) throw err;

				console.log('The file has been saved! ' + process.cwd() + '/nido.js' );
			});
		});
	});

program
	.command('deploy <environment>')
	.description('Deploy the project to the environment (test|production|etc)')
	.action(function(environment){
		api.task('deploy ' + environment, function(){
			var recipe = api.loadRecipe(environment);
			recipe.run('deploy');
		});
	});

program
	.command('server <environment>')
	.description('Setup a new symbiosis server for deployment')
	.action(function(environment) {
		api.task('server ' + environment, function(){
			var recipe = api.loadRecipe(environment);
			recipe.run('server');
		});
	});

program
	.command('deploykey <environment>')
	.description('Setup a new symbiosis server for deployment')
	.action(function(environment) {
		api.task('deploykey ' + environment, function(){
			var recipe = api.loadRecipe(environment);
			recipe.run('deploykey');
		});
	});
	
program
	.command('rollback <environment> <releaseDir>')
	.description('Rollback to release on specified environment e.g. nido rollback production 2015-09-23-183213')
	.action(function(environment, releaseDir){
		api.task('rollback ' + environment, function () {
			// ln -nfs /srv/newiconnet.test.newicon.net/releases/2015-09-23-165942 /srv/newiconnet.test.newicon.net/current
			var recipe = api.loadRecipe(environment);
			recipe.rollbackTo(releaseDir);
		});
	});
	
program
	.command('current <environment>')
	.description('Show the current release folder')
	.action(function(environment){
		api.task('Show the current release folder name', function () {
			var recipe = api.loadRecipe(environment);
			recipe.showCurrentRelease();
		}, {silent:true});
	});
	
program
	.command('releases <environment>')
	.description('Show the list of releases')
	.action(function(environment){
		api.task('Show the list of releases', function () {
			var recipe = api.loadRecipe(environment);
			recipe.showReleases();
		}, {silent:true});
	});

program
	.command('showtasks <environment>')
	.description('Show the tasks available to run for this environment')
	.action(function(environment){
		api.task('Show tasks available for environment: ' + environment, function () {
			var recipe = api.loadRecipe(environment);
			for (var prop in recipe.tasks) {
				console.log(prop)
			}
		});
	});
	
program
	.command('config <environment>')
	.description('Show the config for the specified environment')
	.action(function(environment){
		api.task('Show config', function () {
			var recipe = api.loadRecipe(environment);
			recipe.showConfig();
		}, {silent:true});
	});

program
	.command('pulldb <environment>')
	.description('Pulls the database from the specified environment into you local development environment')
	.action(function(environment){
		api.task('Pulling database from ' + environment, function () {
			var recipe = api.loadRecipe(environment);
			recipe.run('pulldb');
		});
	});

program
	.command('localconfig')
	.description('Display nido local config store info')
	.action(function() {
		let cs =  new Configstore('newicon');
		console.log("file : "+cs.path);
		console.log(cs.all);
	});

program
	.command('pushdb <environment>')
	.description('Push the local database to the specified environment')
	.action(function(environment) {
		api.task('Pushing database to ' + environment, function () {
			var recipe = api.loadRecipe(environment);
			recipe.run('pushdb');
		});
	});

program
	.command('anonymisedb <environment>')
	.description('Anonymise specified data in the local (only) database')
	.action(function(environment) {
		api.task('Anonymise local database', function () {
			var recipe = api.loadRecipe(environment);
			recipe.run('anonymisedb');
		});
	});

program
	.command('pullmedia <environment>')
	.description('Pulls the media assets from the specified environment into you local development environment')
	.action(function(environment){
		api.task('Pulling media from ' + environment, function () {
			var recipe = api.loadRecipe(environment);
			recipe.run('pullmedia');
		});
	});
	
program
	.command('pushmedia <environment>')
	.description('Push the local media assets to the specified environment')
	.action(function(environment){
		api.task('Pushing media to ' + environment, function () {
			var recipe = api.loadRecipe(environment);
			recipe.run('pushmedia');
		});
	});
	
program
	.command('task <task> <environment>')
	.description('Run a specific task by name')
	.action(function(taskName, environment){
		api.task('run task ' + taskName + ' ' + environment, function () {
			var recipe = api.loadRecipe(environment);
			recipe.run(taskName);
		});
	});

program
	.command('serverinfo <server>')
	.description('Get server data')
	.action(function(server){
		api.task('Get server info ' + server, function () {
			console.log(api.getServerData(server));
		});
	});

program
	.command('terms <environment>')
	.description('Show the terms available for an environment')
	.action(function(environment) {
		api.task('Show the terms available for ' + environment, function () {
			var recipe = api.loadRecipe(environment);
			console.log(recipe.remote.getTerms());
		});
	});

program.on('--help', function() {
	console.log('');
	console.log('  Examples:');
	console.log('');
	console.log('    $ nido releases test');
	console.log('    $ nido deploy test');
	console.log('    $ nido pulldb test');
	console.log('');
});

program.on('option:dry-run', function() {
	console.log('\x1b[41m%s\x1b[0m', 'DRY RUN MODE ENABLED')
	// rewrite exec/abort prototypes for ssh/shell to log instead...
	Ssh.prototype._exec = function(command, options) {
		var host = this._connection.config.host;
		options = extend(extend({}, this._options), options); // clone and extend
		if (options.terms) {
			this._terms = extend(this._terms, options.terms);
		}
		command = helpers.replaceTerms(command, this._terms);
		console.log('\x1b[33m%s\x1b[0m', host+ ': '+command);
		return {code: 0, stdout: '',stderr: ''};
	}
	Ssh.prototype.abort = function(message) {
		console.log('\x1b[33m%s\x1b[0m', 'remote ignore abort : '+message);
	}
	Shell.prototype.exec = function(command, options) {
		options = extend(extend({}, this._options), options); // clone and extend
		if (options.terms) {
			this._terms = extend(this._terms, options.terms);
		}
		command = helpers.replaceTerms(command, this._terms);
		console.log('\x1b[33m%s\x1b[0m', 'local: '+command);
		return {code: 0,stdout: '',stderr: ''}
	}
	// rewrite local abort
	Shell.prototype.abort = function(message) {
		console.log('\x1b[33m%s\x1b[0m', 'local ignore abort : '+message);
	}
});

program.on('command:*', function() {
	console.error("\x1b[31mCommand not found '"+program.args.join(' ')+"'\x1b[0m");
});

program.parse(process.argv);

if (!program.args.length) {
	// display help if no commands given to nido
	api.ensureAuthorised();
	program.outputHelp(function(text) {      
		var asciArt  = "\n";
		asciArt += "    ███╗   ██╗██╗██████╗  ██████╗ \n";
		asciArt += "    ████╗  ██║██║██╔══██╗██╔═══██╗ \n";
		asciArt += "    ██╔██╗ ██║██║██║  ██║██║   ██║ \n";
		asciArt += "    ██║╚██╗██║██║██║  ██║██║   ██║ \n";
		asciArt += "    ██║ ╚████║██║██████╔╝╚██████╔╝ \n";
		asciArt += "    ╚═╝  ╚═══╝╚═╝╚═════╝  ╚═════╝  \n";                 
		return asciArt + "\n" + text;
	});
}
