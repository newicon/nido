var mysql = require('mysql2');
var faker = require('faker/locale/en_GB');
var Fibre = require('fibers');
var fs = require('fs');

/**
 * anonymiser.js
 *
 * todo:
 *  - batch updates (for when they get really big)
 *  - ability to ignore newicon emails
 *  - ability to provide a link between related fields (for example a customer_id to link surname & forename)
 *    this would allow:
 *      - more self-consistent data
 *      - ability to link newicon people with their email so their name is not overwritten
 *
 */
let anonymiser = {

	/**
	 * @param config
	 * @param environment
	 */
	anonymiseDb : function(config, environment) {

		let fibre = Fibre.current;

		if (environment !== "local") {
			console.error("Anonymising the database is only permitted locally");
			return;
		}

		if (config.anonymise === undefined) {
			console.error("No 'anonymise' definition was found for environment='"+environment+"'");
			return;
		}
		let anonymiseFilename = process.cwd()+"/"+config.anonymise;
		let anonymiseMetaData = require(anonymiseFilename);

		if (!anonymiser.validateAnonymisationMetaData(anonymiseMetaData)) {
			console.log("Aborting due to anonymisation definition errors");
			return;
		}

		// use a pool since there will be lots of callbacks occuring simultaneously
		let connection = mysql.createPool({
			host : 'localhost',
			user : config.db.username,
			password : config.db.password,
			database : config.db.database,
			port : '3306',
			socketPath : '/Applications/MAMP/tmp/mysql/mysql.sock',
			waitForConnections : true,
			connectionLimit : 20,
		});

		let promises = [];
		anonymiseMetaData.forEach(fieldData => {
			promises.push(anonymiser.anonymiseField(connection,fieldData));
		});

		Promise.all(promises).then(function() {
			connection.end();
			fibre.run();
		}).catch(function(err) {
			console.error(err);
			connection.end();
			fibre.run();
		});

		Fibre.yield();
	},

	/**
	 * anonymise a field or fields
	 *
	 * Notes:
	 *
	 * - fieldData may specify either a single field or multiple fields to update on the table
	 *
	 * - uses the ON DUPLICATE KEY UPDATE technique to bundle the updates
	 *
	 * - unfortunately if there's a clash between the inserted data and the existing data on a field with a unique key
	 *   as the update is being performed then a field constraint error is raised even though there are no duplicates
	 *   in the data used for the update! consequently it's necessary to pre-populate the array used to eliminate the
	 *   duplicates with the values already in the database
	 *
	 * @param connection
	 * @param fieldData
	 * @returns {Promise<any>}
	 */
	anonymiseField : function(connection,fieldData) {

		// determine which field/fields are marked as unique
		var uniqueFields = [];
		if (fieldData.fields !== undefined) {
			fieldData.fields.forEach(multiFieldData=> {
				if (multiFieldData.faker_unique !== undefined && multiFieldData.faker_unique) {
					uniqueFields.push(multiFieldData.field);
				}
			});
		}
		else if (fieldData.faker_unique !== undefined && fieldData.faker_unique) {
			uniqueFields.push(fieldData.field);
		}

		// obtain primary keys and data for any fields marked as unique
		let selectSql= "SELECT " + fieldData.primary_key;
		if (uniqueFields.length>0) {
			selectSql += "," + uniqueFields.join(",");
		}
		selectSql += " FROM " + fieldData.table;
		if (fieldData.select_where !== undefined) {
			selectSql += " WHERE " + fieldData.select_where
		}

		return anonymiser.queryPromise(connection,selectSql).then(function(results) {

			let fields=[];
			let updateData=[];
			let fieldInfo=[];

			if (fieldData.fields !== undefined) {
				// handle faking multiple fields at once
				fields.push(fieldData.primary_key);
				fieldData.fields.forEach(multiFieldData => {
					fields.push(multiFieldData.field);
					fieldInfo.push(
						"'" + multiFieldData.field + "'" +
						(multiFieldData.description!==undefined ? " ("+multiFieldData.description+")" : "")
					);
				});
				updateData = anonymiser.generateMultiFieldUpdateData(fieldData,results,uniqueFields);
			}
			else {
				// handle faking single field
				fields.push(fieldData.primary_key);
				fields.push(fieldData.field);
				fieldInfo.push(
					"'" + fieldData.field + "'" +
					(fieldData.description!==undefined ? " ("+fieldData.description+")" : "")
				);
				updateData = anonymiser.generateFieldUpdateData(fieldData, results, uniqueFields);
			}

			// build the query (note that values have already been escaped!)
			let updateSql = "INSERT INTO " + fieldData.table + "(" + fields.join(", ") + ")\nVALUES\n";
			for (let rowIdx=0; rowIdx<updateData.length; rowIdx++) {
				if (rowIdx>0) {
					updateSql += ",";
				}
				updateSql += "(" + updateData[rowIdx].join(",") + ")";
			}
			updateSql += "\nON DUPLICATE KEY UPDATE ";
			for (let idx=0; idx<fields.length; idx++) {
				if (idx>0) {
					updateSql += ","
				}
				updateSql += fields[idx] + "=VALUES("+fields[idx]+")";
			}

			// perform the update query
			return anonymiser.queryPromise(connection, updateSql).then(function(){
				console.log("anonymised "+updateData.length+" rows in '"+fieldData.table+"' for : "+fieldInfo.join(", "));
			}).catch(function(err) {
				console.log("anonymise update query failed for table '"+fieldData.table+"' fields "+fieldInfo.join(", ")+" = "+err);
				if (err === "Error: Deadlock found when trying to get lock; try restarting transaction") {
					console.log("Try re-running nido, if that doesn't work try splitting up fields definitions in the json");
				}
			});

		}).catch(function(err) {
			console.error(err);
		});
	},

	/**
	 * @param fieldData
	 * @param results
	 * @returns {Array}
	 */
	generateFieldUpdateData : function(fieldData, results, uniqueFields) {
		if (fieldData.fields !== undefined) {
			throw "generateFieldUpdateData can only be used when a single field is specified";
		}

		let updateData=[];
		let uniqueValues = [];

		// prepopulate the unique values with the ones already in the database if necessary
		if (uniqueFields.length>1) {
			throw "multiple unique fields should not be possible when only a single field is being updated!";
		}
		else if (uniqueFields.length===1) {
			results.forEach(result => {
				uniqueValues.push(result[uniqueFields[0]]);
			});
		}

		// generate the faker data for the field
		results.forEach(rowData => {

			let updateDataRow = [];

			updateDataRow.push(rowData[fieldData.primary_key]);

			let fakerValue;
			if (fieldData.faker_unique !== undefined && fieldData.faker_unique) {
				fakerValue = faker.fake(fieldData.faker_action);
				while (uniqueValues.indexOf(fakerValue) > -1) {
					fakerValue = faker.fake(fieldData.faker_action);
				}
				uniqueValues.push(fakerValue);
			}
			else {
				fakerValue  = faker.fake(fieldData.faker_action);
			}

			updateDataRow.push(mysql.escape(fakerValue));

			updateData.push(updateDataRow);
		});
		return updateData;
	},

	/**
	 * @param fieldData
	 * @param results
	 * @returns {Array}
	 */
	generateMultiFieldUpdateData : function(fieldData, results, uniqueFields) {
		if (fieldData.fields === undefined) {
			throw "generateMultiFieldUpdateData can only be used when a multiple fields are specified";
		}

		let updateData = [];
		let uniqueValues = {};

		// prepopulate the unique values with the ones already in the database if necessary
		if (uniqueFields.length>0) {
			uniqueFields.forEach(field => {
				results.forEach(result => {
					if (uniqueValues[field] === undefined) {
						uniqueValues[field] = [];
					}
					uniqueValues[field].push(result[field]);
				});
			});
		}

		// generate the faker data for all the fields
		results.forEach(rowData => {

			let updateDataRow = [];

			updateDataRow.push(rowData[fieldData.primary_key]);

			fieldData.fields.forEach( function(multiFieldData,idx) {

				let fakerValue;
				if (multiFieldData.faker_unique !== undefined && multiFieldData.faker_unique) {
					if (uniqueValues[multiFieldData.field] === undefined) {
						uniqueValues[multiFieldData.field] = [];
					}
					fakerValue = faker.fake(multiFieldData.faker_action)
					while(uniqueValues[multiFieldData.field].indexOf(fakerValue)>-1) {
						fakerValue = faker.fake(multiFieldData.faker_action);
					}
					uniqueValues[multiFieldData.field].push(fakerValue);
				}
				else {
					fakerValue  = faker.fake(multiFieldData.faker_action);
				}

				updateDataRow.push(mysql.escape(fakerValue));
			});

			updateData.push(updateDataRow);
		});
		return updateData;
	},

	/**
	 * @param connection
	 * @param query
	 * @returns {Promise<any>}
	 */
	queryPromise : function(connection, query) {
		return new Promise(function(resolve, reject) {
			connection.query(query, function (err, results) {
				if (err) {
					return reject(err);
				}
				return resolve(results);
			});
		});
	},

	/**
	 * @param anonymisationData
	 * @returns {boolean}
	 */
	validateAnonymisationMetaData : function(anonymisationData) {
		let index=0;
		let hasErrors=false;
		anonymisationData.forEach( fieldData => {
			if (fieldData["table"] === undefined) {
				console.error("Item "+index+" missing required definition for 'table'");
				hasErrors=true;
			}
			if (fieldData["primary_key"] === undefined) {
				console.error("Item "+index+" missing required definition for 'table'");
				hasErrors=true;
			}
			if (fieldData["fields"] === undefined) {
				if (fieldData["field"] === undefined) {
					console.error("Item "+index+" missing required definition for 'field'");
					hasErrors=true;
				}
				if (fieldData["faker_action"] === undefined) {
					console.error("Item "+index+" missing required definition for 'faker_action'");
					hasErrors=true;
				}
				else {
				 	var test = faker.fake(fieldData["faker_action"]);
					//console.log(fieldData["faker_action"] + " test = " + test);
				}
			}
			else {
				if (fieldData["field"] !== undefined) {
					console.error("Item "+index+" has entries for both 'field' and 'fields'");
					hasErrors=true;
				}
				fieldData["fields"].forEach(fieldSubData => {
					if (fieldSubData["field"] === undefined) {
						console.error("Item "+index+" fields subsection missing required definition for 'field'");
						hasErrors=true;
					}
					if (fieldSubData["faker_action"] === undefined) {
						console.error("Item "+index+" fields subsection missing required definition for 'faker_action'");
						hasErrors=true;
					}
					else {
						var test = faker.fake(fieldSubData["faker_action"])
						//console.log(fieldSubData["faker_action"] + " test = " + test);
					}
				});
			}
			index++;
		});
		return !hasErrors;
	}
};

module.exports = anonymiser;