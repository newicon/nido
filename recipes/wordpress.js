var recipe = require('./common');
var fs = require('fs');
var helpers = require('../helpers');
var api = require('../api');

// convenience vars:
var set = recipe.set;
var task = recipe.task;

set('sharedDirs', [{
	"uploads": "public/htdocs/wp-content/uploads"
}]);

set('writableDirs', [{
	"uploads": 'public/htdocs/wp-content/uploads'
}]);

set('webRoot', 'public/htdocs');
set('configFile', 'public/htdocs/wp-config.php');

/**
 * The main deployment function
 * @returns {undefined}
 */
task('deploy', [
	'deploy:prebuild',
	'deploy:gitcheck',
	'deploy:prepare',
	'deploy:release',
	'deploy:config',
	'deploy:wordfenceSetSharedWritable',
	'deploy:W3TCSetSharedWritable',
	'deploy:shared',
	'deploy:writable',
	'deploy:htaccess',
	'deploy:symlink',
	'deploy:clean',
	'createdb'
]);

/**
 * =====================================================================================================================
 * ::: nido pulldb environment ::: Pull the database and media from an environment.  Set up local
 * =====================================================================================================================
 *
 * Called via nido pulldb environment
 * ```nido pulldb environment```
 * will pull the database from the specified environment into the local environment
 * correct the urls and import into the local environment
 * @return void
 *
 */
task('pulldb', [
	'pulldb:dumpRemote',
	'pulldb:transform',
	'pulldb:importRemoteToLocal'
]);

task('pulldb:transform', function(remote, local) {
	var domainRemote = this.config.domain;
	var domainLocal = api.getConfigFor('local', recipe).domain;
	recipe.local.log('Copy and adapt the remote:' + this.environment + ' database for import into the local database');
	recipe.dbAdapt(domainRemote, domainLocal, 'deploy/remote-dump.sql');
});



task('pushdb:transform', function(remote, local) {
	var domainRemote = this.config.domain;
	var domainLocal = api.getConfigFor('local', recipe).domain;
	recipe.local.log('Copy and adapt the local database for import into the remote:' + this.environment + ' database');
	recipe.dbAdapt(domainLocal, domainRemote, 'deploy/local-dump.sql');
});

/**
 * Append the correct htaccess rules for a WordPress site
 * @return void
 */
task('deploy:htaccess', function() {
	recipe.remote.exec('echo # Creating production .htaccess file');
	recipe.remote.exec('mv -f {releasePath}/public/htdocs/.htaccess_production {releasePath}/public/htdocs/.htaccess');
});

task('deploy:wordfenceSetSharedWritable', function() {
	recipe.remote.exec('echo # BEGIN WordFence');
	recipe.setWordFenceSharedWritableDirs();
	recipe.remote.exec('echo # END WordFence');
});

task('deploy:W3TCSetSharedWritable', function() {
	recipe.remote.exec('echo # W3 Total Cache');
	recipe.setW3TCSharedWritableDirs();
	recipe.remote.exec('echo # W3 Total Cache');
});

/**
 * If we have Wordfence set the required shared and writable directories
 */
recipe.setWordFenceSharedWritableDirs = function(environment) {
	environment = environment || this.environment;
	var config = api.getConfigFor(environment, recipe);
	var wf = true;
	if (typeof this.config.wordfence !== 'undefined' && !this.config.wordfence)
		wf = false;
	if (wf) {
		var tmpDir = {
			"tmp": 'public/htdocs/wp-content/plugins/wordfence/tmp',
			"includeHtaccess": false
		};
		var wfcacheDir = {
			"wfcache": 'public/htdocs/wp-content/wfcache',
			"includeHtaccess": false
		};
		var wflogsDir = {
			"wflogs": 'public/htdocs/wp-content/wflogs',
			"includeHtaccess": false
		};
		recipe.get('sharedDirs').push(tmpDir, wfcacheDir, wflogsDir);
		recipe.get('writableDirs').push(tmpDir, wfcacheDir, wflogsDir);
	} else {
		recipe.local.exec('echo # Wordfence not required / set');
	}
};

/**
 * If we have W3 Total Cache set the required shared and writable directories
 */
recipe.setW3TCSharedWritableDirs = function(environment) {
	environment = environment || this.environment;
	var config = api.getConfigFor(environment, recipe);
	var W3TC = true;
	if (typeof this.config.W3TC !== 'undefined' && !this.config.W3TC)
		W3TC = false;
	if (W3TC) {
		var W3TCcacheDir = {
			"cache": 'public/htdocs/wp-content/cache',
			"includeHtaccess": false
		};
		var W3TCconfig = {
			"w3tc-config": 'public/htdocs/wp-content/w3tc-config',
			"includeHtaccess": false
		};
		recipe.get('sharedDirs').push(W3TCcacheDir, W3TCconfig);
		recipe.get('writableDirs').push(W3TCcacheDir, W3TCconfig);
	} else {
		recipe.local.exec('echo # W3 Total Cache not required / set');
	}

}

/**
 * Get the contents of the wordpress configuration file for the current target environment
 * or the environment specified. If the environment paramter is not specified the default
 * behaviour is to use the current target environment defined by flightplan e.g. fly something:test = test environment
 * @param environment string | undefined the environment whose config to use to generate the content. If
 * undefined uses the current target environment
 * @return string escaped for use within shell command e.g. echo ""
 */
recipe.getConfigFileContents = function(environment) {
	environment = environment || this.environment;
	var config = api.getConfigFor(environment, recipe);

	// Set the admin ssl flag
	var ssl_admin_flag = true;
	if (typeof this.config.httpsAdmin !== 'undefined' && !this.config.httpsAdmin)
		ssl_admin_flag = false;
	var cache_flag = true;
	if (typeof this.config.W3TC !== 'undefined' && !this.config.W3TC)
		cache_flag = false;
	wp_memory_limit_output = "";
	if (typeof this.config.wpconfig.wp_memory_limit !== 'undefined' && this.config.wpconfig.wp_memory_limit)
		wp_memory_limit_output = "define('WP_MEMORY_LIMIT', '" + this.config.wpconfig.wp_memory_limit + "');\n";
	var output = "<?php\n";
	output += "define('WP_CACHE'," + cache_flag + ");\n";
	output += "define('DB_NAME', '{db.database}');\n";
	output += "define('DB_USER', '{db.username}');\n";
	output += "define('DB_PASSWORD', '{db.password}');\n";
	output += "define('DB_HOST', '{db.host}');\n";
	output += "define('DB_CHARSET', 'utf8');\n";
	output += "define('DB_COLLATE', '');\n";
	output += "define('AUTH_KEY',         '" + helpers.escape(this.config.wpconfig.auth_key) + "');\n";
	output += "define('SECURE_AUTH_KEY',  '" + helpers.escape(this.config.wpconfig.secure_auth_key) + "');\n";
	output += "define('LOGGED_IN_KEY',    '" + helpers.escape(this.config.wpconfig.logged_in_key) + "');\n";
	output += "define('NONCE_KEY',        '" + helpers.escape(this.config.wpconfig.nonce_key) + "');\n";
	output += "define('AUTH_SALT',        '" + helpers.escape(this.config.wpconfig.auth_salt) + "');\n";
	output += "define('SECURE_AUTH_SALT', '" + helpers.escape(this.config.wpconfig.secure_auth_salt) + "');\n";
	output += "define('LOGGED_IN_SALT',   '" + helpers.escape(this.config.wpconfig.logged_in_salt) + "');\n";
	output += "define('NONCE_SALT',       '" + helpers.escape(this.config.wpconfig.nonce_salt) + "');\n";
	output += "define('FORCE_SSL_ADMIN'," + ssl_admin_flag + ");\n";
	output += wp_memory_limit_output;
	output += "\\$table_prefix  = 'wp_';\n";
	output += "define('WP_DEBUG', false);\n";
	output += "if ( !defined('ABSPATH') )\n";
	output += " define('ABSPATH', dirname(__FILE__) . '/');\n";
	output += "require_once(ABSPATH . 'wp-settings.php');";
	var ret = helpers.replaceTerms(output, config);
	return ret;
};

// prepareFor remote then it must be importing the local database into remote database
// if prepareFor local then it must be the importing the remote database into the local
recipe.mysqlProcessDumpFile = function(dumpFile, prepareFor) {
	var domainRemote = this.config.domain;
	var domainLocal = api.getConfigFor('local', recipe).domain;
	if (prepareFor == 'remote') {
		// going from local to remote
		recipe.local.log('Copy and adapt the local database for import into the remote:' + this.environment + ' database');
		recipe.dbAdapt(domainLocal, domainRemote, dumpFile);
	} else {
		recipe.local.log('Copy and adapt the remote:' + this.environment + ' database for import into the local database');
		// going from remote to local
		recipe.dbAdapt(domainRemote, domainLocal, dumpFile);
	}
};

/**
 * Adapt a wordpress database sql dump by replacing the old_url (url strings from the target db)
 * with new_url the url for the destination db. e.g. wordpresstemplate.com to wordpress.local
 * @param old_url string the string to search form
 * @param new_url string the string to replace the search string with
 * @param file file path to the db dump sql file
 * @return void
 */
recipe.dbAdapt = function(old_url, new_url, file) {

	var Fiber = require('fibers');
	var fiber = Fiber.current;
	var readline = require('readline');
	var ProgressBar = require('progress');
	var adaptWriteFile = require('path').dirname(file)+'/db_dump_adapt_stream';

	// 'w' flag: Open file for writing. The file is created (if it does not exist) or truncated (if it exists).
	var writeStream = fs.createWriteStream(adaptWriteFile, {flags: 'w'})
	var lineReader = readline.createInterface({input: fs.createReadStream(file)});
	var totalSize = 0;
	fs.stat(file, function (err, stats) {
		totalSize = stats.size;

		var bar = new ProgressBar('Converting dump [:bar] :percent :current/:total', { total: totalSize, width: 20 });
		lineReader.on('line', function (line) {
			writeStream.write(recipe.replaceUrls(old_url, new_url, line)+"\n");
			bar.tick(Buffer.byteLength(line, 'utf8'));
		}).on('close', () => {
			console.log("\n"+'rename '+file+' to '+file+'_original'+"\n");
			fs.renameSync(file, file+'_original');
			console.log("\n"+'rename '+adaptWriteFile+' to '+file+"\n");
			fs.renameSync(adaptWriteFile, file);
			fiber.run();
		});

	});

	return Fiber.yield();

};

/**
 * Replace Url strings in the content
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param content string the string to search
 * @see this.replaceUrlsInString();
 * @see this.replaceUrlsInSerialized()
 * @source grunt-wordpress-deploy
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrls = function(search, replace, content) {
	content = this.replaceUrlsInSerialized(search, replace, content);
	content = this.replaceUrlsInString(search, replace, content);
	return content;
};

/**
 * Replace Url strings in php serialized strings
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param string string the string to search
 * @source grunt-wordpress-deploy
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrlsInSerialized = function(search, replace, string) {
	var length_delta = search.length - replace.length;
	var search_regexp = new RegExp(search, 'g');
	// Replace for serialized data
	var matches, length, delimiter, old_serialized_data, target_string, new_url, occurences;
	var regexp = /s:(\d+):([\\]*['"])(.*?)\2;/g;
	while (matches = regexp.exec(string)) {
		old_serialized_data = matches[0];
		target_string = matches[3];
		// If the string contains the url make the substitution
		if (target_string.indexOf(search) !== -1) {
			occurences = target_string.match(search_regexp).length;
			length = matches[1];
			delimiter = matches[2];
			// Replace the url
			new_url = target_string.replace(search_regexp, replace);
			length -= length_delta * occurences;
			// Compose the new serialized data
			var new_serialized_data = 's:' + length + ':' + delimiter + new_url + delimiter + ';';
			// Replace the new serialized data into the dump
			string = string.replace(old_serialized_data, new_serialized_data);
		}
	}
	return string;
};

/**
 * Replace Url strings in a string
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param string string the string to search
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrlsInString = function(search, replace, string) {
	var regexp = new RegExp('(?!' + replace + ')(' + search + ')', 'g');
	return string.replace(regexp, replace);
};

module.exports = recipe;