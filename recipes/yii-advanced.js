var recipe = require('./common')
,	set = recipe.set
,   task = recipe.task;

set('sharedDirs', [
	// 'frontend/uploads', (not used)
	'frontend/runtime',
	'frontend/web/assets'
]);

set('numReleasesToKeep', (recipe.environment == 'test') ? 1 : 4);

set('sharedFiles', []);

set('writableDirs', [
	//'frontend/uploads', (not used)
	'frontend/runtime',
	'frontend/web/assets'
]);

set('webRoot', 'frontend/web');
set('configFile', 'common/config/main-local.php');

task('deploy', [
	'deploy:prepare',
	'deploy:release',
	'deploy:config',
	'deploy:shared',
	'deploy:writable',
	'deploy:vendor',
	'deploy:migrate',
	'deploy:symlink',
	'deploy:clean'
]);

task('deploy:migrate', function(){
	recipe.remote.exec('php {releasePath}/yii migrate up --interactive=0');
});

/**
 * Get the contents of the configuration file for the current target environment
 * or the environment specified. If the environment paramter is not sepficied the default
 * behaviour is to use the current target environment defined by flightplan e.g. fly something:test = test environment
 * @param environment string | undefined the environment whose config to use to generate the content. If 
 * undefined uses the current target environment
 * @return string escaped for use within shell command e.g. echo ""
 */
task('deploy:config', function(){
	var output = "<?php\n";
	output += "return array (\n";
	output += "	'name' => '{siteName}',\n";
	output += "	'timezone' => 'Europe/London',\n";
	output += "	'components' => array (\n";
	output += "		'db' => array (\n";
	output += "			'class' => 'yii\\db\\Connection',\n";
	output += "			'dsn' => 'mysql:host={db.host};dbname={db.database}',\n";
	output += "			'username' => '{db.username}',\n";
	output += "			'password' => '{db.password}',\n";
	output += "			'charset' => 'utf8',\n";
	output += "			'tablePrefix' => '',\n";
	output += "		)\n";
	output += "	)\n";
	output += ");\n";
	recipe.remote.exec('echo "' + output + '" > {releasePath}/{configFile}', {silent: true});
});

module.exports = recipe;