var recipe = require('./common');
var fs = require('fs');
var helpers = require('../helpers');
var api = require('../api');

// convienience vars:
var set = recipe.set;
var task = recipe.task;

/**
 * Additional config options required:
 * 
 * unsecureBaseUrl: 'http://permagard.local/',
 * secureBaseUrl: 'http://permagard.local/',
 * crypt: 'f1a4fff85d3028fabfa4a7af8c0dae95',
 */

set('sharedDirs', [{
	"media": "public/htdocs/media"
}, {
	"var": "public/htdocs/var"
}, {
	"sitemap": "public/htdocs/sitemap"
}]);
set('writableDirs', ['public/htdocs/media', 'public/htdocs/var', 'public/htdocs/sitemap']);
set('webRoot', 'public/htdocs');
set('configFile', 'public/htdocs/app/etc/local.xml');

/**
 * The main deployment function
 * @returns {undefined}
 */
task('deploy', [
	'deploy:prebuild',
	'deploy:gitcheck',
	'deploy:prepare',
	'deploy:multisite',
	'deploy:release',
	'deploy:config',
	'deploy:htaccess',
	'deploy:shared',
	'deploy:writable',
	'deploy:symlink',
	'deploy:clean',
	'deploy:clearversion',
	'deploy:cache:clear'
]);

/**
 * DEPLOY TASK: Create multisite secondary domains and link to the primary installation
 * -----------------------------------------------------------------------------
 */
task('deploy:multisite', function(remote, local) {
	var config = api.getConfigFor(recipe.environment, recipe);
	if (typeof config.multisite !== 'undefined' && config.multisite) {
		var primaryDeployPath = config.deployPath;
		recipe.remote.log('Create multisite secondary domains and link to the primary installation');
		config.websites.forEach(function(website) {
			// If the website is the primary domain then skip it
			var siteDeployPath = website.deployPath;
			if (siteDeployPath == primaryDeployPath)
				return;
			// Create the directory and sym link to the primary installation
			recipe.remote.log('Creating ' + website.secureBaseUrl + ' domain and linking to the primary installation');
			recipe.remote.exec('mkdir -p ' + siteDeployPath + '/public');
			recipe.remote.exec('ln -nfs ' + primaryDeployPath + '/public/htdocs ' + siteDeployPath + '/public/htdocs');
		});
	}
});

/**
 * DEPLOY TASK: Insert the correct htaccess rules for a Magento site
 * @return void
 */
task('deploy:htaccess', function() {
	recipe.remote.exec('echo # Creating ' + this.environment + ' environment .htaccess file');
	recipe.remote.exec('mv -f {releasePath}/public/htdocs/htaccess_env/.htaccess_' + this.environment + ' {releasePath}/public/htdocs/.htaccess');
});

/**
 * DEPLOY TASK: Configure with Redis
 * @param  {[type]} ) {	recipe.run('gen:configredis');} [description]
 * @return {[type]}   [description]
 */
task('deploy:configredis', function() {
	recipe.run('gen:configredis');
});

/**
 * DEPLOY TASK: Clear the magento cache
 */
task('deploy:cache:clear', function() {
	recipe.clearCache('remote');
});
task('deploy:cache:clearredis', function() {
	recipe.clearRedisCache('remote');
});

/**
 * DEPLOY TASK: Remove files and directories that can be used to compromise Magento
 */
task('deploy:clearversion', function() {
	recipe.remote.exec("rm -f {releasePath}/public/htdocs/LICENSE.html");
	recipe.remote.exec("rm -f {releasePath}/public/htdocs/LICENSE.txt");
	recipe.remote.exec("rm -f {releasePath}/public/htdocs/LICENSE_AFL.txt");
	recipe.remote.exec("rm -f {releasePath}/public/htdocs/RELEASE_NOTES.txt");
	// Remove the various environment .htacess files used for deployment
	recipe.remote.exec("rm -R {releasePath}/public/htdocs/htaccess_env");
});

/**
 * DEPLOY TASK: Generate the config file
 */
task('gen:config', function(remote, local) {
	recipe.generateLocalXmlConfigFile('remote', false);
});
task('gen:configredis', function(remote, local) {
	recipe.generateLocalXmlConfigFile('remote', true);
});

/**
 * TASK: Push the database from local to a remote environment 
 */
task('pushdb', function(remote, local) {
	// Dump and pull the database
	recipe.pushDb();
	// Update remote database fields
	recipe.updateDbFields('remote');
	// Clear the remote magento cache
	recipe.clearCache('remote');
});

/**
 * TASK: Pull the database from a environment to local
 */
task('pulldb', function(remote, local) {
	// Dump and pull the database
	recipe.pullDb();
	// Update local database fields
	recipe.updateDbFields('local');
	// Generate the local local config xml file
	recipe.generateLocalXmlConfigFile('local');
	// Clear the local magento cache
	recipe.clearCache('local');
});


/**
 * TASK: Update the database with specific mysql queries based on the environment
 * @param env string - 'local' | 'test' | 'staging' | 'production'
 */
task('updateDbFields', function(env) {
	recipe.updateDbFields(env);
});

/**
 * TASK: Generate the local.xml config file similar to deploy:config
 * @param env string - 'local' | 'test' | 'staging' | 'production'
 */
task('generateLocalXmlConfigFile', function(env) {
	recipe.generateLocalXmlConfigFile(env);
});

/**
 * TASK: Clear the magento cache
 */
task('clearCache', function(env) {
	recipe.clearCache(env);
});

/**
 * Update the database with specific mysql queries based on the environment
 * @param env string - 'local' | 'remote'
 */
recipe.updateDbFields = function(env) {
	var environment = env == 'remote' ? recipe.environment : env;
	var config = api.getConfigFor(environment, recipe);

	var envCache = env == 'local' ? 0 : 1;
	var srv = env == 'local' ? recipe.local : recipe.remote;
	var pwd = env == 'local' ? '' : '-p{db.password}';
	var cmd;

	// If this is a magento multisite installation update the base urls for each website scope
	if (typeof config.multisite !== 'undefined' && config.multisite) {
		config.websites.forEach(function(website) {
			var scopeId = website.scopeId;
			var unsecureBaseUrl = website.unsecureBaseUrl;
			var secureBaseUrl = website.secureBaseUrl;

			srv.log('Replace DB Urls for (un)secureBaseUrls in core_config_data"');
			cmd = "mysql -u{db.username} " + pwd + " '{db.database}' -e\
				'UPDATE core_config_data SET value = \"" + unsecureBaseUrl + "\" WHERE core_config_data.path = \"web/unsecure/base_url\" AND core_config_data.scope_id = " + scopeId + ";\
				UPDATE core_config_data SET value = \"" + secureBaseUrl + "\" WHERE core_config_data.path = \"web/secure/base_url\" AND core_config_data.scope_id = " + scopeId + "'";
			cmd = helpers.replaceTerms(cmd, config);
			srv.exec(cmd);
		});
		// Else just update the default base urls
	} else {
		srv.log('Replace DB Urls for (un)secureBaseUrl in core_config_data"');
		cmd = "mysql -u{db.username} " + pwd + " '{db.database}' -e\
				'UPDATE core_config_data SET value = \"{unsecureBaseUrl}\" WHERE core_config_data.path = \"web/unsecure/base_url\";\
				UPDATE core_config_data SET value = \"{secureBaseUrl}\" WHERE core_config_data.path = \"web/secure/base_url\"'";
		cmd = helpers.replaceTerms(cmd, config);
		srv.exec(cmd);
	}

	// Update the robots meta
	if (typeof config.robots !== 'undefined') {
		srv.log('Update system config default robots for production or non production environment: in core_config_data"');
		cmd = "mysql -u{db.username} " + pwd + " '{db.database}' -e\
			'UPDATE core_config_data SET value = \"{robots}\" WHERE core_config_data.path = \"design/head/default_robots\"'";
		cmd = helpers.replaceTerms(cmd, config);
		srv.exec(cmd);
	}

	// Update system settings to enable the magento cache
	srv.log('Update system settings to enable the magento cache ');
	cmd = "mysql -u{db.username} " + pwd + " '{db.database}' -e\
		'UPDATE core_cache_option SET value=" + envCache + "'";
	cmd = helpers.replaceTerms(cmd, config);
	srv.exec(cmd);
}

/**
 * Clear the magento cache
 * @param env string - 'local' | 'remote'
 */
recipe.clearCache = function(env) {
	var srv = env == 'local' ? recipe.local : recipe.remote;
	srv.log('Clearing the Magento cache');
	if (env == 'local') {
		// Clear the cache
		srv.exec('rm -rf public/htdocs/var/cache/*');
		// Clear the sessions
		srv.log('Clearing the sessions');
		srv.exec('rm -rf public/htdocs/var/session/*');
	} else {
		srv.exec('rm -rf {deployPath}/public/htdocs/var/cache/*');
		srv.exec("cd {deployPath}/public/htdocs && php -r \"require_once 'app/Mage.php'; umask(0); Mage::app()->cleanCache();\"");
	}
}

/**
 * Clear the Redis cache
 * @param env string - 'local' | 'remote'
 */
recipe.clearRedisCache = function(env) {
	var srv = env == 'local' ? recipe.local : recipe.remote;
	if (env != 'local') {
		srv.log('Clearing the Redis cache');
		srv.exec('redis-cli flushall');
	}
}

/**
 * Generate the local.xml config file similar to deploy:config
 * this works for all environments including local,
 * deploy:config generates the config file in the release folder before it is released.
 * this function generates the config file after it is released.
 * @param env string - 'local' | 'remote'
 */
recipe.generateLocalXmlConfigFile = function(env, redis) {
	var environment = env == 'remote' ? recipe.environment : env;
	var config = api.getConfigFor(environment, recipe);
	var configFile = mageUtil.getConfigFileContents(env, redis);
	if (env == 'local') {
		recipe.local.log('Generating the local.xml config file for the local environment');
		recipe.local.exec('echo \'' + configFile + '\' > ' + config.deployPath + '/{configFile}');
	} else {
		recipe.remote.log('Generating the local.xml config file for the remote environment');
		recipe.remote.exec('echo \'' + configFile + '\' > {releasePath}/{configFile}');
	}
}

/**
 * Magento Utilities 
 */
var mageUtil = {

	/**
	 * Get the config file contents for a specfied environment
	 * @param env string - 'local' | 'test' | 'staging' | 'production'
	 */
	getConfigFileContents: function(env, redis) {
		return mageUtil.getLocalXmlContent(env, redis);
	},

	/**
	 * Generates the local.xml file contents for a given environment.
	 * @param env string - 'local' | 'test' | 'staging' | 'production'
	 * @return string template
	 */
	getLocalXmlContent: function(env, redis) {
		var config = api.getConfigFor(env, recipe);
		var template = mageUtil.getLocalXmlTemplate(env, redis);
		config.date = new Date().toISOString();
		template = helpers.replaceTerms(template, config);
		return template;
	},

	/**
	 * Returns the app/etc/local.xml template.
	 * The indentation is funny here because of the way the JS interprets
	 * the string; i.e. it's reading the tabs after the initial line and thus
	 * the indentation in the written file remains correct.
	 * IF, thinking 'ew, that looks gross' (and I sympathise), you re-indent
	 * the below templating code, your file will be improperly indented.
	 * Correct indentation in local.xml > non-gross indentation in flightplan.js.
	 *
	 * @return string
	 */
	getLocalXmlTemplate: function(env, redis) {
		if (env == 'local' || redis == false) {
			return '<?xml version=\"1.0\"?>\n\
<!-- This config.xml was autogenerated by nido -->\n\
<config>\n\
    <global>\n\
        <install>\n\
            <date><![CDATA[{date}]]></date>\n\
        </install>\n\
        <crypt>\n\
            <key><![CDATA[{crypt}]]></key>\n\
        </crypt>\n\
        <disable_local_modules>false</disable_local_modules>\n\
        <resources>\n\
            <db>\n\
                <table_prefix><![CDATA[]]></table_prefix>\n\
            </db>\n\
            <default_setup>\n\
                <connection>\n\
                    <host><![CDATA[{db.host}]]></host>\n\
                    <username><![CDATA[{db.username}]]></username>\n\
                    <password><![CDATA[{db.password}]]></password>\n\
                    <dbname><![CDATA[{db.database}]]></dbname>\n\
                    <initStatements><![CDATA[SET NAMES utf8]]></initStatements>\n\
                    <model><![CDATA[mysql4]]></model>\n\
                    <type><![CDATA[pdo_mysql]]></type>\n\
                    <pdoType><![CDATA[]]></pdoType>\n\
                    <active>1</active>\n\
                </connection>\n\
            </default_setup>\n\
        </resources>\n\
        <session_save><![CDATA[files]]></session_save>\n\
    </global>\n\
    <admin>\n\
        <routers>\n\
            <adminhtml>\n\
                <args>\n\
                    <frontName><![CDATA[{adminHtmlFrontName}]]></frontName>\n\
                </args>\n\
            </adminhtml>\n\
        </routers>\n\
    </admin>\n\
</config>';
		} else {
			return '<?xml version=\"1.0\"?>\n\
<!-- This config.xml was autogenerated by nido -->\n\
<config>\n\
    <global>\n\
        <install>\n\
            <date><![CDATA[{date}]]></date>\n\
        </install>\n\
        <crypt>\n\
            <key><![CDATA[{crypt}]]></key>\n\
        </crypt>\n\
        <disable_local_modules>false</disable_local_modules>\n\
        <resources>\n\
            <db>\n\
                <table_prefix><![CDATA[]]></table_prefix>\n\
            </db>\n\
            <default_setup>\n\
                <connection>\n\
                    <host><![CDATA[{db.host}]]></host>\n\
                    <username><![CDATA[{db.username}]]></username>\n\
                    <password><![CDATA[{db.password}]]></password>\n\
                    <dbname><![CDATA[{db.database}]]></dbname>\n\
                    <initStatements><![CDATA[SET NAMES utf8]]></initStatements>\n\
                    <model><![CDATA[mysql4]]></model>\n\
                    <type><![CDATA[pdo_mysql]]></type>\n\
                    <pdoType><![CDATA[]]></pdoType>\n\
                    <active>1</active>\n\
                </connection>\n\
            </default_setup>\n\
        </resources>\n\
        <session_save><![CDATA[files]]></session_save>\n\
        <redis_session>                       <!-- All options seen here are the defaults -->\n\
            <host>127.0.0.1</host>            <!-- Specify an absolute path if using a unix socket -->\n\
            <port>6379</port>\n\
            <password></password>             <!-- Specify if your Redis server requires authentication -->\n\
            <timeout>2.5</timeout>            <!-- This is the Redis connection timeout, not the locking timeout -->\n\
            <persistent></persistent>         <!-- Specify unique string to enable persistent connections. E.g.: sess-db0; bugs with phpredis and php-fpm are known: https://github.com/nicolasff/phpredis/issues/70 -->\n\
            <db>1</db>                        <!-- Redis database number; protection from accidental loss is improved by using a unique DB number for sessions -->\n\
            <compression_threshold>2048</compression_threshold>  <!-- Set to 0 to disable compression (recommended when suhosin.session.encrypt=on); known bug with strings over 64k: https://github.com/colinmollenhour/Cm_Cache_Backend_Redis/issues/18 -->\n\
            <compression_lib>gzip</compression_lib>              <!-- gzip, lzf, lz4 or snappy -->\n\
            <log_level>1</log_level>               <!-- 0 (emergency: system is unusable), 4 (warning; additional information, recommended), 5 (notice: normal but significant condition), 6 (info: informational messages), 7 (debug: the most information for development/testing) -->\n\
            <max_concurrency>7</max_concurrency>                 <!-- maximum number of processes that can wait for a lock on one session; for large production clusters, set this to at least 10% of the number of PHP processes -->\n\
            <break_after_frontend>5</break_after_frontend>       <!-- seconds to wait for a session lock in the frontend; not as critical as admin -->\n\
            <break_after_adminhtml>30</break_after_adminhtml>\n\
            <first_lifetime>600</first_lifetime>                 <!-- Lifetime of session for non-bots on the first write. 0 to disable -->\n\
            <bot_first_lifetime>60</bot_first_lifetime>          <!-- Lifetime of session for bots on the first write. 0 to disable -->\n\
            <bot_lifetime>7200</bot_lifetime>                    <!-- Lifetime of session for bots on subsequent writes. 0 to disable -->\n\
            <disable_locking>0</disable_locking>                 <!-- Disable session locking entirely. -->\n\
            <min_lifetime>60</min_lifetime>                      <!-- Set the minimum session lifetime -->\n\
            <max_lifetime>2592000</max_lifetime>                 <!-- Set the maximum session lifetime -->\n\
        </redis_session>\n\
        <cache>\n\
            <backend>Cm_Cache_Backend_Redis</backend>\n\
            <backend_options>\n\
                <server>127.0.0.1</server> <!-- or absolute path to unix socket -->\n\
                <port>6379</port>\n\
                <persistent></persistent> <!-- Specify unique string to enable persistent connections. E.g.: sess-db0; bugs with phpredis and php-fpm are known: https://github.com/nicolasff/phpredis/issues/70 -->\n\
                <database>0</database> <!-- Redis database number; protection against accidental data loss is improved by not sharing databases -->\n\
                <password></password> <!-- Specify if your Redis server requires authentication -->\n\
                <force_standalone>0</force_standalone>  <!-- 0 for phpredis, 1 for standalone PHP -->\n\
                <connect_retries>1</connect_retries>    <!-- Reduces errors due to random connection failures; a value of 1 will not retry after the first failure -->\n\
                <read_timeout>10</read_timeout>         <!-- Set read timeout duration; phpredis does not currently support setting read timeouts -->\n\
                <automatic_cleaning_factor>0</automatic_cleaning_factor> <!-- Disabled by default -->\n\
                <compress_data>1</compress_data>  <!-- 0-9 for compression level, recommended: 0 or 1 -->\n\
                <compress_tags>1</compress_tags>  <!-- 0-9 for compression level, recommended: 0 or 1 -->\n\
                <compress_threshold>20480</compress_threshold>  <!-- Strings below this size will not be compressed -->\n\
                <compression_lib>gzip</compression_lib> <!-- Supports gzip, lzf, lz4 (as l4z) and snappy -->\n\
                <use_lua>0</use_lua> <!-- Set to 1 if Lua scripts should be used for some operations -->\n\
            </backend_options>\n\
        </cache>\n\
        <full_page_cache>\n\
            <backend>Cm_Cache_Backend_Redis</backend>\n\
            <backend_options>\n\
                <server>127.0.0.1</server> <!-- or absolute path to unix socket -->\n\
                <port>6379</port>\n\
                <persistent></persistent> <!-- Specify unique string to enable persistent connections. E.g.: sess-db0; bugs with phpredis and php-fpm are known: https://github.com/nicolasff/phpredis/issues/70 -->\n\
                <database>2</database> <!-- Redis database number; protection against accidental data loss is improved by not sharing databases -->\n\
                <password></password> <!-- Specify if your Redis server requires authentication -->\n\
                <force_standalone>0</force_standalone>  <!-- 0 for phpredis, 1 for standalone PHP -->\n\
                <connect_retries>1</connect_retries>    <!-- Reduces errors due to random connection failures -->\n\
                <lifetimelimit>57600</lifetimelimit>    <!-- 16 hours of lifetime for cache record -->\n\
                <compress_data>0</compress_data>        <!-- DISABLE compression for EE FPC since it already uses compression -->\n\
            </backend_options>\n\
        </full_page_cache>\n\
    </global>\n\
    <admin>\n\
        <routers>\n\
            <adminhtml>\n\
                <args>\n\
                    <frontName><![CDATA[{adminHtmlFrontName}]]></frontName>\n\
                </args>\n\
            </adminhtml>\n\
        </routers>\n\
    </admin>\n\
</config>';
		}
	},

};
module.exports = recipe;