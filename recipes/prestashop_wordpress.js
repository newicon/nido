var recipe = require('./common');
var fs = require('fs');
var helpers = require('../helpers');
var api = require('../api');

// convenience vars:
var set = recipe.set;
var task = recipe.task;

/**
 * The main deployment function
 * @returns {undefined}
 */
task('deploy', [
	// 'deploy:prebuild',
	'deploy:gitcheck',
	'deploy:prepare',
	'deploy:release',
	'deploy:shared',
	'deploy:writable',
	'deploy:symlink',
	'deploy:clean',
	'clearcaches'
]);

task('pulldb', function(remote, local) {
	recipe.pullDb();
	// we want to regenerate the local config file now.
	// You could run `nido task gen:config local` to generate the config file
	// this is the equivalent of:
	// recipe.setup('local');
	// recipe.run('deploy:setLocalSslDbValues');
});

task('clearcaches', function(remote, local){
	if (recipe.environment==='staging' || recipe.environment==='production') {
		remote.exec('rm -rf {deployPath}/current/www/cache/smarty/cache/*');
		remote.exec('rm -rf {deployPath}/current/www/cache/smarty/compile/*');
		remote.exec('rm -f {deployPath}/current/www/cache/class_index.php');
		remote.exec('rm -rf {deployPath}/current/www/img/tmp/*');
		var clearCachesTask = recipe.get('clearCachesTask');
		if (clearCachesTask) {
			remote.exec(clearCachesTask);
		}
	}
	else {
		local.log('skipping cache clearing in local environment');
	}
});


// task('deploy:setLocalSslDbValues', function() {
// 	var terms = recipe.config;
// 	recipe.local.log(JSON.stringify(terms));
// });

recipe.pullDb = function() {
	var localConfig = api.getConfigFor('local', recipe);
	
	if (localConfig.db.wordpress !== undefined && localConfig.db.wordpress.database !== undefined) {
		recipe.mysqlRemoteDump('deploy/remote-dump-wp.sql', 'wordpress');
		recipe.mysqlImportIntoLocal('deploy/remote-dump-wp.sql', 'wordpress');
	}

	if (localConfig.db.prestashop !== undefined && localConfig.db.prestashop.database !== undefined) {
		recipe.mysqlRemoteDump('deploy/remote-dump-ps.sql', 'prestashop');
		recipe.mysqlImportIntoLocal('deploy/remote-dump-ps.sql', 'prestashop');
	}

};

/**
 * @param {type} dumpFile
 * @param {string} prepareFor
 * @param {Function} function to call when dump file is successfully processed - passed the file path of the processed dump file
 * @param {string|null} dbId  indicate which database to use from the config.
 * 		(only used if more than one database needed for the deployment)
 * 		e.g. if config looks like this
 * 			local: {
 *				env:'dev',
 *				...
 *				db: {
 *					prestashop: {
 *						database: 'prestashop_db',
 *						username: 'prestashop_user',
 *						password: 'prestashop_password'
 *					},
 *					wordpress: {
 *						database: 'wordpress_db',
 *						username: 'wordpress_user',
 *						password: 'wordpress_password'
 *					}
 *				},
 *				...
 *			},
 *		then you need to set the dbId to either 'prestashop' or 'wordpress'.
 * @returns {undefined}
 */
recipe.mysqlProcessDumpFile = function(dumpFile, prepareFor, callback, dbId = null) {
	// prepareFor remote then it must be importing the local database into remote database
	// if prepareFor local then it must be the importing the remote database into the local
	var domainRemote = this.config.domain;
	var domainLocal = api.getConfigFor('local', recipe).domain;
	// var domainLocal = (dbId === null) ? localConfig.db.domain : localConfig.db[dbId].domain;
	recipe.local.log('dbId = ' + dbId);
	if (prepareFor === 'remote') {
		// if dbId is null, assume prestashop
		if (!dbId || dbId === 'prestashop') {
			// going from local to remote
			recipe.local.log('Copy and adapt the local database for import into the remote:' + this.environment + ' database');
			recipe.dbAdapt(domainLocal, domainRemote, dumpFile);
			// TODO: Speed up the process by running sql commands remotely to set these instead of find and replace. This would also mean you dont need the dbId as the dbAdapt command would be the same for presta and wordpress
			recipe.dbAdapt("'PS_SSL_ENABLED','0'", "'PS_SSL_ENABLED','1'", dumpFile);
			recipe.dbAdapt("'PS_SSL_ENABLED_EVERYWHERE','0'", "'PS_SSL_ENABLED_EVERYWHERE','1'", dumpFile);
		}
		else if (dbId === 'wordpress') {
			recipe.local.log('Copy and adapt the local database for import into the remote:' + this.environment + ' database');
			// going from local to remote
			recipe.dbAdapt(domainLocal, domainRemote, dumpFile);

		} else {
			throw "unknown database type";
		}
	} else {
		if (dbId === 'prestashop') {
			recipe.local.log('Copy and adapt the remote:' + this.environment + ' database for import into the local database');
			// going from remote to local
			recipe.dbAdapt(domainRemote, domainLocal, dumpFile);
			recipe.dbAdapt("'PS_SSL_ENABLED','1'", "'PS_SSL_ENABLED','0'", dumpFile);
			recipe.dbAdapt("'PS_SSL_ENABLED_EVERYWHERE','1'", "'PS_SSL_ENABLED_EVERYWHERE','0'", dumpFile);
		}
		else if (dbId === 'wordpress') {
			recipe.local.log('Copy and adapt the remote:' + this.environment + ' database for import into the local database');
			// going from remote to local
			recipe.dbAdapt(domainRemote, domainLocal, dumpFile);

		} else {
			throw "unknown database type"
		}
	}
};

/**
 * Adapt a wordpress database sql dump by replacing the old_url (url strings from the target db)
 * with new_url the url for the destination db. e.g. wordpresstemplate.com to wordpress.local
 * @param old_url string the string to search form
 * @param new_url string the string to replace the search string with
 * @param file file path to the db dump sql file
 * @return void
 */
recipe.dbAdapt = function(old_url, new_url, file) {

	var Fiber = require('fibers');
	var fiber = Fiber.current;
	
	
	var readline = require('readline');
	var ProgressBar = require('progress');
	var adaptWriteFile = require('path').dirname(file)+'/db_dump_adapt_stream';
	

	// 'w' flag: Open file for writing. The file is created (if it does not exist) or truncated (if it exists).
	var writeStream = fs.createWriteStream(adaptWriteFile, {flags: 'w'})  
	var lineReader = readline.createInterface({input: fs.createReadStream(file)});
	var totalSize = 0;
	fs.stat(file, function (err, stats) {
		totalSize = stats.size;
	
		var bar = new ProgressBar('Converting dump [:bar] :percent :current/:total', { total: totalSize, width: 20 });
		lineReader.on('line', function (line) {
			writeStream.write(recipe.replaceUrls(old_url, new_url, line)+"\n");
			bar.tick(Buffer.byteLength(line, 'utf8'));
		}).on('close', () => {
			console.log("\n"+'rename '+file+' to '+file+'_original'+"\n");
			fs.renameSync(file, file+'_original');
			console.log("\n"+'rename '+adaptWriteFile+' to '+file+"\n");
			fs.renameSync(adaptWriteFile, file);
			fiber.run();
		});

	});
	
	return Fiber.yield();

};

/**
 * Replace Url strings in the content
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param content string the string to search
 * @see this.replaceUrlsInString();
 * @see this.replaceUrlsInSerialized()
 * @source grunt-wordpress-deploy
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrls = function(search, replace, content) {
	content = this.replaceUrlsInSerialized(search, replace, content);
	content = this.replaceUrlsInString(search, replace, content);
	return content;
};

/**
 * Replace Url strings in php serialized strings
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param string string the string to search
 * @source grunt-wordpress-deploy
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrlsInSerialized = function(search, replace, string) {
	var length_delta = search.length - replace.length;
	var search_regexp = new RegExp(search, 'g');
	// Replace for serialized data
	var matches, length, delimiter, old_serialized_data, target_string, new_url, occurences;
	var regexp = /s:(\d+):([\\]*['"])(.*?)\2;/g;
	while (matches = regexp.exec(string)) {
		old_serialized_data = matches[0];
		target_string = matches[3];
		// If the string contains the url make the substitution
		if (target_string.indexOf(search) !== -1) {
			occurences = target_string.match(search_regexp).length;
			length = matches[1];
			delimiter = matches[2];
			// Replace the url
			new_url = target_string.replace(search_regexp, replace);
			length -= length_delta * occurences;
			// Compose the new serialized data
			var new_serialized_data = 's:' + length + ':' + delimiter + new_url + delimiter + ';';
			// Replace the new serialized data into the dump
			string = string.replace(old_serialized_data, new_serialized_data);
		}
	}
	return string;
};

/**
 * Replace Url strings in a string
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param string string the string to search
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrlsInString = function(search, replace, string) {
	var regexp = new RegExp('(?!' + replace + ')(' + search + ')', 'g');
	return string.replace(regexp, replace);
};

module.exports = recipe;
