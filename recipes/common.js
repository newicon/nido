var recipe = require('../recipe');
var api = require('../api');
var set = recipe.set;
var task = recipe.task;
var chalk = require('chalk');
var anonymiser = require('../lib/anonymiser');
var axios = require('axios');
var Fiber = require('fibers');
var _ = require('lodash');

// specify an array of relative paths
// from the projects root directory
// to become shared or/and writable
// with no begining slash
// You can specify an object if you would like the shared path to be different
// to the project path with the property being the shared path and the value
// being the project path
// e.g:
// recipe.set('sharedDirs', ['app/runtime', 'app/web/assets', {'image/uploads' : 'app/protected/image/uploads'}]);
set('sharedDirs', []);
set('sharedFiles', []);

/**
 * set "writableDirs to an array relative paths
 * from the projects root directory to make writable
 */
set('writableDirs', []);
set('writableFiles', []);

// specify an array of project relative paths to syncronise as media
// via the pullmedia and pushmedia commands
set('media', []);

// The relative path in the project to the web root. When going to the domain you will serve Recipe folder.
// leave Recipe blank to set the web root to the root project path e.g. the current forlder.
set('webRoot', 'public/htdocs');

// For Composer installation. Like SYMFONY_ENV=prod
set('envVars', '');

set('numReleasesToKeep', 5);

/**
 * TASK: prepare - prepare for deployment. Gets the git id to deploy and set up the server
 * -----------------------------------------------------------------------------
 * And gets the git head id that requires deployment
 * @returns {undefined}
 */
task('deploy:prepare', function(remote, local) {
	recipe.remote.exec('dsp="$(df -h / | tail -1 | tr -s \' \' | cut -d \' \' -f 1,4,5)"; echo "Disk usage : $dsp";');
	if (recipe.environment === 'production') {
		var answer = recipe.remote.prompt('Ready to deploy to production? [yes]');
		if (answer.indexOf('yes') === -1) {
			recipe.remote.abort('Cancelled');
		}
	}
	set('gitBranch', recipe.get('gitBranch') ? 'refs/heads/' + recipe.get('gitBranch') : 'HEAD');
	var gitResult = recipe.local.exec('git ls-remote {repo} {gitBranch}', {
		silent: true
	}).stdout;
	if (gitResult == null) {
		local.abort('The git branch "' + recipe.get('gitBranch') + '" does not seem to exist.');
	}
	var gitHead = gitResult.replace(recipe.get('gitBranch'), '').trim();
	set('gitHead', gitHead);
	recipe.remote.log('prepare to deploy git head: ' + recipe.get('gitHead'));
	recipe.remote.exec('mkdir -p {deployPath}/public');
	recipe.remote.exec('mkdir -p {deployPath}/releases');
	recipe.remote.exec('mkdir -p {deployPath}/shared');
	// Automate database and database user creation
	// TODO: check if the database and database user exists?

	// make a note globally of the current release so we can suggest how to rollback at the end.
	if (recipe.remote.directoryExists('{deployPath}/current', recipe.config)) {
		var currentReleaseDir = recipe.getCurrentReleaseDetails().currentDir;
		set('rollbackToRelease', currentReleaseDir);
	} else {
		recipe.remote.log('No current release exists, so you can not rollback');
	}
});

/**
 * TASK: Run the local build script
 * @returns {undefined}
 */
task('deploy:prebuild', recipe.preBuild);

/**
 * TASK: Set writable dirs
 * @returns {undefined}
 */
task('deploy:writable', recipe.writable);

/**
 * Checks for local changes.
 * If git has detected changes then possibly a build script needs to run
 * Or you have not properly checked your changes in.  
 * Nido always deploys from git
 * @returns {undefined}
 */
task('deploy:gitcheck', function(remote, local) {
	var output = local.exec('git status --porcelain');
	// if you have unstaged changes
	if (output.stdout !== null) {
		local.abort('You have local unstaged changes. Test these changes (possibly caused by the build script) then commit to git.');
	}
});

/**
 * TASK: createdb - create a database and a user with privelages for this database on the specified environment
 */
task('createdb', function(remote, local, remoteRoot) {
	var rootMysqlPassword = recipe.getRootMysqlPassword(recipe.config.server);
	remoteRoot.exec('mysql -h {db.host} -u root -p' + rootMysqlPassword + ' -e \'CREATE DATABASE IF NOT EXISTS {db.database}; GRANT ALL ON `{db.database}`.* TO "{db.username}"@"localhost" IDENTIFIED BY "{db.password}";\'');
});

/**
 * TASK: shared - create symlinks to shared directories and files
 * -----------------------------------------------------------------------------
 * @see "sharedDirs" and "sharedFiles"
 * Create shared directories, symlink project shared paths specified in "sharedDirs" to the shared directory
 * And symlink shared files in project specified by "sharedFiles" config option
 * @returns {undefined}
 */
task('deploy:shared', recipe.shared);

/**
 * TASK: deploy:writable - create writable directories
 * -----------------------------------------------------------------------------
 * Make writable directories specifed in config option "writableDirs"
 * Make the uploads, runtime and assets folders writable. Set the group to www-data
 * For Recipe to work admin must be a member of www-data
 * on the server edit the /etc/group file
 * change the line similar to "www-data:x:503:" to "www-data:x:503:admin" (basically add admin on the end)
 * refer to wiki article https://hub.newicon.net/wiki-article/secure-deployment-permissions-for-web-accessible-uploads-and-media-folders
 * now admin has the ability to change files and folders to belong to the www-data group.
 * we want to trust www-data as little as possible as Recipe is what php runs as.  But admin we can trust more.
 * @see "wirtableDirs"
 * @return  {undefined}
 */
task('deploy:writable', recipe.writable);

/**
 * TASK: deploy:release - Main deplyments process: check out git and copy to release folder
 * -----------------------------------------------------------------------------
 * get the latest git, pop it in repo.  Then copy accross to the new release folder
 * @returns {undefined}
 */
task('deploy:release', function(remote, local) {
	try {
		remote.exec('if [ -d "{deployPath}/shared/repo" ]; then cd {deployPath}/shared/repo && git fetch -q origin && git fetch --tags -q origin && git reset -q --hard {gitHead} && git clean -q -d -x -f; else git clone -q {repo} {deployPath}/shared/repo && cd {deployPath}/shared/repo && git checkout -q -b deploy {gitHead}; fi');
		// copy the repository into the release folder
		remote.exec('cp -RPp {deployPath}/shared/repo {deployPath}/releases/{releaseDir}');
		// place a file in the new release folder with the git revision (gitHead) id in it
		remote.exec('echo {gitHead} > {deployPath}/releases/{releaseDir}/REVISION');
	} catch (e) {
		remote.log('release failed, rolling back');
		var rollbackToRelease = recipe.get('rollbackToRelease');
		remote.exec('rm -rf {deployPath}/releases/{releaseDir}');
		remote.exec('ln -nfs {deployPath}/releases/'+rollbackToRelease+' {deployPath}/current');
		process.exit();
	}
});

/**
 * TASK: deploy:symlink - Creates the symlink to the new deployed release folder
 * -----------------------------------------------------------------------------
 * @returns {undefined}
 */
task('deploy:symlink', recipe.symlink);

/**
 * TASK: config - generate the project configuration file
 * -----------------------------------------------------------------------------
 * @see "getConfigFileContents"
 * Generate the wp-config file with the db credentials for wordpress
 * @returns {undefined}
 */
task('deploy:config', function() {
	recipe.run('gen:config');
});

if (recipe.environment == 'production') {
	set('composerOptions', 'install --no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction');
} else {
	set('composerOptions', 'install --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction');
}

/**
 * TASK: deploy:vendor - Installing composer bases vendors tasks.
 * -----------------------------------------------------------------------------
 * Installing vendors tasks.
 * @see set('composerOptions')
 * @return {undefined}
 */
task('deploy:vendor', function() {
	recipe.remote.log('Installing vendors');
	if (recipe.remote.commandExists('composer')) {
		$composer = 'composer';
	} else {
		recipe.remote.exec("cd {releasePath} && curl -sS https://getcomposer.org/installer | php");
		$composer = 'php composer.phar';
	}
	$composerEnvVars = recipe.get('envVars') ? 'export ' + recipe.get('envVars') + ' &&' : '';
	recipe.remote.exec("cd {releasePath} && " + $composerEnvVars + " " + $composer + " {composerOptions}");
});

/**
 * TASK: deploy:clean - clean up after deployment.
 * -----------------------------------------------------------------------------
 * Typically remove old release folders
 * @returns {undefined}
 */
task('deploy:clean', function() {
	var files = recipe.releasesArray();
	if (files[files.length - 1] == '') {
		files.pop();
	}
	var deleteDirectories = files.splice(recipe.get('numReleasesToKeep'), files.length);
	var currentRelease = recipe.getCurrentReleaseDetails().currentDir;
	if (deleteDirectories.length > 0) {
		for (i = 0; i < deleteDirectories.length; i++) {
			// we are doing an rf now so just double check we don't have an empty string
			// as Recipe would try to remove the releases folder
			if (deleteDirectories[i] == '') {
				continue;
			}
			// double check we are not about to delete the current release
			if (deleteDirectories[i] != currentRelease) {
				recipe.remote.exec('rm -rf {deployPath}/releases/' + deleteDirectories[i]);
			}
		}
	} else {
		recipe.remote.log('No release folders to clean up, currently ' + files.length);
	};
	if (recipe.get('rollbackToRelease')) {
		recipe.remote.getLogger().info('You can rollback using ' + chalk.yellow('nido rollback ' + recipe.environment + ' ' + recipe.get('rollbackToRelease')));
	}
	let postDeploy = recipe.get('postDeploy');
	if (postDeploy) {
		recipe.remote.log('Running post deployment task(s):');
		if (!Array.isArray(postDeploy)) postDeploy = [postDeploy];
		postDeploy.forEach(function(remoteTask) {
			recipe.remoteRoot.exec(remoteTask);
		});
	}
});

task('pullmedia', recipe.pullMedia);

task('pushmedia', recipe.pushMedia);

/**
 * =====================================================================================================================
 * ::: nido pulldb environment ::: Pull the database and media from an environment.  Set up local
 * =====================================================================================================================
 *
 * Called via nido pulldb environment
 * ```nido pulldb environment```
 * will pull the database from the specified environment into the local environment
 * correct the urls and import into the local environment
 * @return void
 *
 */
task('pulldb', [
	'pulldb:dumpRemote',
	'pulldb:transform',
	'pulldb:importRemoteToLocal'
]);

task('pulldb:dumpRemote', function(remote, local) {
	recipe.mysqlRemoteDump('deploy/remote-dump.sql');
	remote.log('Dumped remote "'+recipe.environment+'" database to: deploy/remote-dump.sql');
});

// stub function to be overriden if any changes need to be made to the dumped file before importing into the local
// database - for example with wordpress changing url and environment variables
task('pulldb:transform', function() {});

/**
 * Import a previously dumped file into the local database
 */
task('pulldb:importRemoteToLocal', function(remote, local) {
	recipe.mysqlImportIntoLocal('deploy/remote-dump.sql');
	// we want to regenerate the local config file now.
    // You could run `nido task gen:config local` to generate the config file
    // this is the equivalent of:
	recipe.setup('local');
	recipe.run('deploy:config');
});
//======================================================================================================================


/**
 * =====================================================================================================================
 * ::: nido pushdb environment
 * =====================================================================================================================
 * Called via nido pushdb environment
 *
 * ```nido pushdb test```
 *
 * Takes your current local database. Formats the url's to correct for the target environment
 * and then imports the database on the target environment.  Can be very usefull to push local databse changes to a
 * remote test server. etc.
 *
 * @return void
 */
task('pushdb', [
	'pushdb:warning',
	'pushdb:dumpLocal',
	'pushdb:transform',
	'pushdb:importLocalToRemote'
]);

task('pushdb:warning', function() {
	// prompt when deploying to a specific target
	if (recipe.environment === 'production') {
		var input = recipe.local.prompt('You are about to push your local database to production, are you sure you want to do this? [yes]');
		if (input.indexOf('yes') === -1) {
			recipe.local.abort('Cancelled, phew I thought you were about to completely �!%! everything');
		}
	}
});

task('pushdb:dumpLocal', function(remote, local) {
	recipe.mysqlLocalDump('deploy/local-dump.sql');
	remote.log('Dumped local database to: deploy/local-dump.sql');
});

// transform stub can be overridden in recipes
task('pushdb:transform', function() {});

// import the local dump file into the remote database
task('pushdb:importLocalToRemote', function(remote, local){
	recipe.mysqlImportIntoRemote('deploy/local-dump.sql');
});


// similar to deploy:config
// gen:config overwrites the current released config for the specified environment
// this works for all environments including local,
// deploy:config generates the config file in the release folder before it is released.
// this function generates the config file after it is released.
task('gen:config', function(remote, local) {
	var configFile = recipe.getConfigFileContents();
	recipe.remote.exec('echo "' + configFile + '" > {releasePath}/{configFile}', {
		silent: true
	});
});

task('anonymisedb', function(remote, local) {
	var config = api.getConfigFor(recipe.environment, recipe);
	anonymiser.anonymiseDb(config, this.environment);
});

task('server', [
	'server:install_tools',
	'server:admin_to_www-data',
	'server:apache_mod_expires',
	'server:keygen',
	'server:bitbucket',
	'server:bitbucket_deploy_key'
]);

task('server:install_tools', function(remote, local, remoteRoot) {
	remoteRoot.exec('apt-get -y install git vim curl gawk');
}, 'install git vim curl gawk');

task('server:bitbucket', function(remote, local, remoteRoot) {
	try {
		remoteRoot.exec('ssh -T -o "StrictHostKeyChecking no" -o PasswordAuthentication=no hg@bitbucket.org 2>/dev/null');
	} catch (err) {
		console.log(err);
		// carry on as this is supposed to error
	}
}, 'Add BitBucket to known hosts');

task('server:admin_to_www-data', function(remote, local, remoteRoot) {
	remoteRoot.exec('/usr/sbin/usermod -a -G www-data admin');
},'Add the admin user to the www-data user group');

task('server:apache_mod_expires', function(remote, local, remoteRoot) {
	remoteRoot.exec('a2enmod expires');
});


task('server:keygen', function(remote, local) {
	// if id_rsa.pub does not exist then create a new key
	try {
		remote.exec('[ ! -f /srv/.ssh/id_rsa.pub ] && ssh-keygen -q -P "" -f /srv/.ssh/id_rsa');
	} catch (err) {
		// check the file was created and if so carry on
		remote.exec('[ -f /srv/.ssh/id_rsa.pub ]');
	}
}, 'Generates a private and public key for use with deploy keys');

task('server:bitbucket_deploy_key', function(remote, local) {
	// generate deploy key
	let output = remote.exec('cat ~/.ssh/id_rsa.pub');
	let sshKey = output.stdout.replace(/\n/g, '');
	// add public key to bitbucket repository
	var accounts = api.getAccountData({"url": "bitbucket.org", "name": "nido-bitbucket-password"});
	if (accounts.length === 0) {
		throw new Error('No bitbucket account data found')
	}
	let username = accounts[0].username;
	let password = accounts[0].password;

	let server = recipe.config.server;
	let repo = recipe.config.repo;
	let auth = Buffer.from(username+':'+password).toString('base64');
	// get the repository username and repo name from the bitbucket git repository path
	let match = repo.match(/git@bitbucket.org:(.*).git/);
	let repoSlug = match[1];
	const postDeployKey = function() {
		var fiber = Fiber.current;
		axios.post(
			'https://api.bitbucket.org/2.0/repositories/'+repoSlug+'/deploy-keys',
			{"key": sshKey, "label": server},
			{auth: {username:username, password:password}}
		).then(response => {
			local.log('Successfully added deploy key for ' + server);
			fiber.run(response);
		}).catch(error=>{
			if (error.response) {
				if (_.has(error, 'response.data.key.0.message')) {
					if (_.get(error, 'response.data.key.0.message') === 'Someone has already added that access key to this repository.') {
						local.log('Successfully added deploy key for ' + server);
					}
 				} else {
					local.error(error.response.status + ' ' + error.response.data);
					local.error('Failed to add key');
					console.log(error.response.data);
				}
			} else {
				console.error(error);
			}
			fiber.run(error);
		});
		return Fiber.yield();
	};
	try {
		postDeployKey();
	} catch (err) {
		console.log('Bitbucket deployment may have succeeded. Check the repository to see if the key has been added or not');
	}
}, 'Generated a ssh key on the server and adds the public key to the bitbucket repositories access keys a deploy key - id_rsa.pub must already exist - see "deploy:keygen');

module.exports = recipe;
