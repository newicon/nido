var recipe = require('./common');
var set = recipe.set;
var task = recipe.task;
var logger = require('../cmd/logger')();


set('numReleasesToKeep', (recipe.environment == 'test') ? 1 : 4);
set('writableDirs', ['public/htdocs/assets', 'app/runtime']);
set('sharedDirs', ['uploads']);
set('media', ['uploads']);
set('webRoot', 'public');

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:config',
    'deploy:shared',
    'deploy:writable',
    'createdb',
    //'deploy:migrate',
    'deploy:symlink',
	'deploy:clean'
]);

task('deploy:config', function(remote, local){
    var cmd = "";
    recipe.useCorrectConfigFile();
});

recipe.useCorrectConfigFile = function(environment) {
    environment = environment || this.environment;
    var dbName = this.config.db.database;
    if (dbName.includes("staging")) {
        cmd = "find . -type f ! -name '*_staging*' -delete && mv main_staging.php main.php";
        recipe.remote.exec('cd {releasePath}/app/config && '+ cmd);
    }
    else if (dbName.includes("production")) {
        cmd = "find . -type f ! -name '*_production*' -delete && mv main_production.php main.php";
        recipe.remote.exec('cd {releasePath}/app/config && '+ cmd);
    }

};


module.exports = recipe;