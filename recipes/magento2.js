var recipe = require('./common');
var fs = require('fs');
var helpers = require('../helpers');
var api = require('../api');

/**
 * recipe settings
 */
recipe.set('webRoot', 'pub');
recipe.set('sharedDirs',['pub/media','pub/sitemap','pub/robots']);
recipe.set('writableDirs', ['generated', 'var', 'pub/static/_cache','pub/sitemap','pub/robots']);
recipe.set('configFile', 'app/etc/env.php');
recipe.set('configTemplateFile', 'app/etc/env.php.template');

// see https://stackoverflow.com/questions/9446783/remove-definer-clause-from-mysql-dumps#answer-9447215
recipe.set('pushdbMysqldumpPipe',"sed -e 's/DEFINER[ ]*=[ ]*[^*]*\\*/\\*/'");
recipe.set('pulldbMysqldumpPipe',"sed -e 's/DEFINER[ ]*=[ ]*[^*]*\\*/\\*/'");

recipe.set('mysqldumpParams','--single-transaction');

/**
 * main deployment function
 */
recipe.task('deploy', [
	//'deploy:prebuild',
	'deploy:gitcheck',
	'deploy:prepare',
	'deploy:multisite',
	'deploy:release',
	'deploy:config',
	// 'deploy:htaccess',
	'deploy:shared',
	'deploy:writable',
	'deploy:bin-magento',
	'deploy:symlink',
	'deploy:clean',
]);

/**
 * task : setup multi site symlinks
 */
recipe.task('deploy:multisite', function(remote,local) {
	var config = M2.getConfig();
	if (('multisite' in config) && config.multisite) {
		var primaryDeployPath = config.deployPath;
		remote.log('Create multisite secondary domains and link to the primary installation');
		config.websites.forEach(function(website) {
			// If the website is the primary domain then skip it
			var siteDeployPath = website.deployPath;
			if (siteDeployPath === primaryDeployPath)
				return;
			// Create the directory and sym link to the primary installation
			remote.log('Creating ' + website.secureBaseUrl + ' domain and linking to the primary installation');
			remote.exec('mkdir -p ' + siteDeployPath + '/public');
			remote.exec('ln -nfs ' + primaryDeployPath + '/public/htdocs ' + siteDeployPath + '/public/htdocs');
		});
	}
});

/**
 * task : set up magento using /bin/magento
 * https://devdocs.magento.com/guides/v2.3/performance-best-practices/deployment-flow.html
 */
recipe.task('deploy:bin-magento', function(remote, local) {
	var config = M2.getConfig();

	if (recipe.environment==='local') {
		local.log("Skipping local m2 setup : use bin/magento instead!");
		return;
	}

	// ensure any new extensions are installed ... this is necessary since the db (which contains local upgrade info
	// has probably not been pushed)
	M2.remoteBinMagento('setup:upgrade');

	// ensure the config files are up to date
	M2.remoteBinMagento('app:config:import');

	// deploy static content
	var staticDeployParams = '';
	if (('themes' in config) ) {
		if (Array.isArray(config.themes)) {
			for (var idx in config.themes) {
				if (config.themes.hasOwnProperty(idx)) {
					staticDeployParams += '--theme ' + config.themes[idx] + ' ';
				}
			}
		}
		else {
			local.error("Expected an array for 'themes'");
		}
	}
	if ('themeLocale' in config ) {
		if (typeof config.themeLocale=='string') {
			staticDeployParams += config.themeLocale;
		}
		else {
			local.error("Expected a string for 'themeLocale'");
		}
	}
	M2.remoteBinMagento('setup:static-content:deploy -f '+staticDeployParams);

	// ensure catalog images pre-cache is populated
	M2.remoteBinMagento('catalog:image:resize');

	// set deployment mode only if necessary (probably will be if local
	var diCompileWasRun = false;
	if ('deployMode' in config) {
		var validDeployModes = ['default','developer','production'];
		if (!validDeployModes.includes(config.deployMode)) {
			remote.abort("Unrecognised deployment mode '"+config.deployMode+"', valid modes are : '"+validDeployModes.join("', '")+"'");
		}
		else {
			// only update the deployment mode if it's different (since it's rather time consuming)
			var result = M2.remoteBinMagento('deploy:mode:show');
			// response looks like this "Current application mode: developer. (Note: Environment variables may override this value.)"
			var matches = result.stdout.match(/:[^\.?]*/m);
			if (matches===null) {
				remote.abort('failed to get magento 2 deploy mode from server');
			}
			else if (0 in matches) {
				var currentMode = matches[0].substring(1).trim();
				if (currentMode !== config.deployMode) {
					M2.remoteBinMagento('deploy:mode:set '+config.deployMode);
					if (config.deployMode==='production') {
						diCompileWasRun=true;
					}
				}
			}
			else {
				remote.error('Failed to identify the deploy mode!');
			}
		}
	}

	if (!diCompileWasRun) {
		// compile di
		M2.remoteBinMagento('setup:di:compile');

		// the magento 2 docs recommend running the following, but composer isn't installed on the server and the
		// autoload files are already built (they're in git) and relative to the vendor folder anyway:
		//   composer dump-autoload -o --apcu
	}

	// reindex
	M2.remoteBinMagento('indexer:reindex');

	// remove the existing crontab file because the next command is going to append it
    remote.exec('cd {deployPath}/releases/{releaseDir}');
    try {
        remote.exec('crontab -r');
    } catch (error) {
        // ignore the error so that the script continues to run
    }

	// create or update the cron definitions
	M2.remoteBinMagento('cron:install --force');

	// flush the cache
	M2.remoteBinMagento('cache:flush');
});

/**
 * task : generate config files from template/samples
 */
recipe.task('gen:config', function(remote, local) {

	var config = M2.getConfig();

	if (recipe.environment==='local') {
		// ensure dev js config files are present and correct as they aren't in the repo
		// (this assumes they haven't been monkeyed with!)
		// note that these shouldn't be necessary on staging/live since they are for dev only
		local.exec('cp grunt-config.json.sample grunt-config.json');
		local.exec( 'cp Gruntfile.js.sample Gruntfile.js');
		local.exec('cp package.json.sample package.json');
	}

	// handle generation of the main Magento config file (php)
	var env_file = recipe.get('configFile');
	if (recipe.environment==='local') {
		if (fs.existsSync(env_file)) {
			var res = local.prompt('The local config file ' + env_file + ' already exists ... do you want to overwrite it? [yes/no]');
			if (res.trim().toLowerCase() !== 'yes') {
				local.log('magento will continue to use the existing config file');
				return;
			}
		}
	}

	var env_template_file = recipe.get('configTemplateFile');
	if (!fs.existsSync(env_template_file)) {
		local.error("Unable to build config file - Can't find " + env_template_file);
	} else {
		var env_template_string = fs.readFileSync(env_template_file).toString();
		var configFile = helpers.replaceTermsWithDefaultsSupported(env_template_string, config);

		if (configFile.indexOf('"')>-1) {
			local.error('The config template file cannot contain double quotes : '. env_template_file);
			return;
		}

		if (recipe.environment === 'local') {
			local.log('Generating the Magento config file for the local environment');
			local.exec('echo "' + configFile + '" > {deployPath}/{configFile}');
		} else {
			remote.log('Generating the Magento config file for the remote environment');
			remote.exec('echo "' + configFile + '" > {releasePath}/{configFile}');
		}
	}
});

/**
 * task : push the database
 */
recipe.task('pushdb', function(remote, local) {
	// Dump and push the database
	recipe.pushDb();
	// Update remote database fields
	M2.updateDbFields('remote');
	// Import config data (since env.php may no longer have the correct checksum value in the db)
	M2.remoteBinMagentoCurrent('app:config:import');
	// Clear the remote magento cache
	M2.remoteBinMagentoCurrent('cache:flush');
});

/**
 * task : pull the database
 */
recipe.task('pulldb', function(remote, local) {
	// Dump and pull the database
	recipe.pullDb();
	// Update local database fields
	M2.updateDbFields('local');
	// Generate the local local config files
	recipe.task('gen:config');
	// Import config data (since env.php may no longer have the correct checksum value in the db)
	local.exec('php bin/magento app:config:import');
	// Clear the local magento cache
	local.exec('php bin/magento cache:flush');
});

recipe.task('pushmedia', function(remote, local) {
	recipe.pushMedia(false);

	// the following workaround attempted to fix a static media image paths capitalisation bug
	// however it's insufficient and really the bug needs properly addressing
	// this is left here just as a reminder:
	//
	// var alphabetArray = 'abcdefghijklmnopqrstuvwxyz'.split('');
	// local.log('Symlinking catalog product image cache images to workaround M2 catalog cache path bug...');
	// for (var idx in alphabetArray) {
	// 	var symlinkFrom = '{deployPath}/shared/pub/media/catalog/product/'+alphabetArray[idx].toUpperCase();
	// 	var symlinkTo = '{deployPath}/shared/pub/media/catalog/product/'+alphabetArray[idx];
	// 	remote.exec('if [ -d $(echo '+symlinkFrom+') ]; then ln -nfs '+symlinkFrom+' '+symlinkTo+'; fi');
	// }
});

/**
 * magento 2 helper
 */
var M2 = {

	/**
	 * local config values cache (indexed by the environment name)
	 */
	_config:[],

	/**
	 * returns the merged config for the specified environment name
	 *
	 * @param {string} env
	 * @returns {*}
	 */
	getConfig(env) {
		if (typeof env=='undefined') {
			env = recipe.environment;
		}
		if (typeof M2._config[env] === 'undefined') {
			// use a deep copy to avoid any global state
			var config = JSON.parse(JSON.stringify(recipe.config));
			if (!("environments" in config)) {
				recipe.local.abort("Failed to find 'environments' in the config!");
			}
			if (!env in config.environments) {
				recipe.local.abort("Failed to find environment '"+env+"' in the config!");
			}
			var envParams = config.environments[env];
			for (var key in envParams) {
				if (envParams.hasOwnProperty(key)) {
					config[key] = envParams[key];
				}
			}
			M2._config[env] = config
		}
		return M2._config[env];
	},

	/**
	 * run a remote bin/magento command in the releases dir
	 * @param {string} command
	 * @returns {boolean|RegExpExecArray}
	 */
	remoteBinMagento: function(command) {
		return recipe.remote.exec('cd {deployPath}/releases/{releaseDir} && php ./bin/magento ' + command);
	},

	/**
	 * run a remote bin/magento command in the current dir (i.e. assumes the symlink is already up to date)
	 * @param command
	 */
	remoteBinMagentoCurrent: function(command) {
		return recipe.remote.exec('cd {deployPath}/current && php ./bin/magento ' + command);
	},

	/**
	 * update database fields directly in the db
	 *
	 * @param {string} localOrRemote
	 */
	updateDbFields(localOrRemote) {
		if (localOrRemote!=='local' && localOrRemote!=='remote') {
			recipe.local.abort("value must be either 'local' or 'remote'");
		}
		var env = localOrRemote==='local'
			? 'local'
			: recipe.environment;
		var config = M2.getConfig(env);

		if ('multisite' in config && config.multisite) {
			var isDefault = true;
			config.websites.forEach(function(website) {
				if (isDefault) {
					// assumption : the first website entry in nido.js is the default store
					M2.updateDbUrlsForScope(env, 0, website.unsecureBaseUrl, website.secureBaseUrl);
					isDefault=false;
				}
				M2.updateDbUrlsForScope(env, parseInt(website.scopeId), website.unsecureBaseUrl, website.secureBaseUrl);
			});
		}
		else {
			M2.updateDbUrlsForScope(env,0, website.unsecureBaseUrl, website.secureBaseUrl);
		}
	},

	/**
	 * updates the full set of magento 2 core_config_data entries for urls
	 *
	 * @param {string} env
	 * @param {int} scopeId
	 * @param {string} unsecureUrl
	 * @param {string} secureUrl
	 */
	updateDbUrlsForScope(env, scopeId, unsecureUrl, secureUrl) {
		if (unsecureUrl===null) {
			recipe.local.abort("Sanity check failed 'unsecureUrl' was null");
		}
		if (secureUrl===null) {
			recipe.local.abort("Sanity check failed 'secureUrl' was null");
		}
		// as per normal the pathetic magento documentation provides no useful information about these fields
		M2.updateDbCoreConfigData(env, scopeId,'web/unsecure/base_url', unsecureUrl);
		M2.updateDbCoreConfigData(env, scopeId,'web/unsecure/base_static_url', null);
		M2.updateDbCoreConfigData(env, scopeId,'web/unsecure/base_media_url', null);
		M2.updateDbCoreConfigData(env, scopeId,'web/unsecure/base_web_url', null);
		M2.updateDbCoreConfigData(env, scopeId,'web/unsecure/base_link_url', null);
		M2.updateDbCoreConfigData(env, scopeId,'web/secure/base_url', secureUrl);
		M2.updateDbCoreConfigData(env, scopeId,'web/secure/base_static_url', null);
		M2.updateDbCoreConfigData(env, scopeId,'web/secure/base_media_url', null);
		M2.updateDbCoreConfigData(env, scopeId,'web/secure/base_web_url', null);
		M2.updateDbCoreConfigData(env, scopeId,'web/secure/base_link_url', null);
	},

	/**
	 * update a specific magento 2 core_config_data value
	 *
	 * @param {string} env
	 * @param {int} scopeId
	 * @param {string} path
	 * @param {null|string} value
	 */
	updateDbCoreConfigData(env, scopeId, path, value) {
		if (path.indexOf("'")>-1) {
			recipe.local.abort("the path should not contain single quotes : "+path);
		}
		if (value!=null && value.indexOf("'")>-1) {
			recipe.local.abort("the value should not contain single quotes : "+value);
		}
		if (!Number.isInteger(scopeId)) {
			recipe.local.abort("scopeId must be an integer : "+scopeId);
		}
		var setValue = (value==null) ? "null" : "'"+value+"'";
		var sql = "UPDATE core_config_data SET value="+setValue+" WHERE path='"+path+"' AND scope_id="+scopeId;
		M2.mysqlExec(sql, env);
	},

	/**
	 * execute a raw sql command in the specified environment
	 *
	 * take care ... no escaping is performed ... however if anyone is performing sql injection at this stage you've got
	 * bigger problems => your build process has already been compromised
	 *
	 * @param {string} sql
	 * @param {undefined|string} env
	 */
	mysqlExec(sql, env) {
		var config = M2.getConfig(env);
		if (sql.indexOf('"')>-1) {
			recipe.local.abort("the sql to be executed cannot contain double quotes : "+sql);
		}
		var mysqlPasswordParam = (config.db.password) ? '-p{db.password} ' : '';
		var mysqlExecString =  helpers.replaceTerms("mysql -u{db.username} " + mysqlPasswordParam + "'{db.database}' -e ", config);
		var sqlExec = mysqlExecString+' "'+sql+'"';
		if (env === 'local') {
			recipe.local.exec(sqlExec)
		}
		else {
			recipe.remote.exec(sqlExec);
		}
	}
};

module.exports = recipe;
