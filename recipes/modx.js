var recipe = require('./common');
var fs = require('fs');
var helpers = require('../helpers');
var api = require('../api');

// convenience vars:
var set = recipe.set;
var task = recipe.task;

set('sharedDirs', [{
	"uploads": "public/htdocs/uploads"
},{
	"cache": "public/htdocs/core/cache"
// },{
	// "core": "public/htdocs/core"
// },{
	// "connectors": "public/htdocs/connectors"
// },{
	// "manager": "public/htdocs/manager"
}]);

set('media', [{
	"uploads": "public/htdocs/uploads"
}]);

set('writableDirs', [{
	"uploads": "public/htdocs/uploads"
},{
	"root": "public/htdocs/"
},{
	"cache": "public/htdocs/core/cache"
},{
	"export": "public/htdocs/core/export"
},{
	"packages": "public/htdocs/core/packages"
},{
	"manager": "public/htdocs/manager"
},{
	"connectors": "public/htdocs/connectors"
}]);

set('writableFiles', [
	"public/htdocs/core/config/config.inc.php"
]);

set('webRoot', 'public/htdocs');
set('coreConfigFile', 'public/htdocs/core/config/config.inc.php');
set('rootConfigFile', 'public/htdocs/config.core.php');
set('connectorsConfigFile', 'public/htdocs/connectors/config.core.php');
set('managerConfigFile', 'public/htdocs/manager/config.core.php');
/**
 * The main deployment function
 * @returns {undefined}
 */
task('deploy', [
	'deploy:prebuild',
	'deploy:gitcheck',
	'deploy:prepare',
	'deploy:release',
	'deploy:config',
	'deploy:shared',
	'deploy:writable',
	'deploy:htaccess',
	'deploy:symlink',
	'deploy:clean'
]);

/**
 * Append the correct htaccess rules for a WordPress site
 * @return void
 */
task('deploy:htaccess', function() {
	recipe.remote.exec('echo # Creating '+ this.environment +' .htaccess file');
	recipe.remote.exec('mv -f {releasePath}/{webRoot}/htaccess_env/.htaccess.' + this.environment + ' {releasePath}/{webRoot}/.htaccess');
});

/**
 * DEPLOY TASK: Generate the config file
 */
task('gen:config', function() {
	var configFile = recipe.getConfigFileContents();
	var connectorsConfigFile = recipe.getConnectorsConfigFileContents();
	recipe.remote.exec('echo "' + configFile + '" > {releasePath}/{coreConfigFile}');
	recipe.remote.exec('echo "' + connectorsConfigFile + '" > {releasePath}/{rootConfigFile}');
	recipe.remote.exec('echo "' + connectorsConfigFile + '" > {releasePath}/{connectorsConfigFile}');
	recipe.remote.exec('echo "' + connectorsConfigFile + '" > {releasePath}/{managerConfigFile}');
});

/**
 * TASK: Push the database from local to a remote environment 
 */
task('pushdb', function(remote, local) {
	// Dump and pull the database
	recipe.pushDb();
	// Update remote database fields
	recipe.updateDbFields('remote');
});

/**
 * TASK: pull the database from a remote to local environment 
 */
task('pulldb', function(remote, local) {
	// Dump and pull the database
	recipe.pullDb();
	// Update remote database fields
	recipe.updateDbFields('local');
});


recipe.getConnectorsConfigFileContents = function(env) {
	env = env || this.environment;
	var config = api.getConfigFor(env, recipe);
	var output = "<?php\n";
	output += "define('MODX_CORE_PATH', '{base_path}/core/');\n";
	output += "define('MODX_CONFIG_KEY', 'config');\n";
	output += "?>";
	var ret = helpers.replaceTerms(output, config);
	return ret;
}

recipe.getConfigFileContents = function(env) {
	env = env || this.environment;
	var config = api.getConfigFor(env, recipe);
	var output = "<?php\n";
	output += "/**\n";
	output += " *  MODX Configuration file\n";
	output += " */\n";
	output += "\\$database_type = 'mysql';\n";
	output += "\\$database_server = '{db.host}';\n";
	output += "\\$database_user = '{db.username}';\n";
	output += "\\$database_password = '{db.password}';\n";
	output += "\\$database_connection_charset = 'utf8';\n";
	output += "\\$dbase = '{db.database}';\n";
	output += "\\$table_prefix = 'modx_';\n";
	output += "\\$database_dsn = 'mysql:host=localhost;dbname={db.database};charset=utf8';\n";
	output += "\\$config_options = array (\n";
	output += "  'override_table' => 'MyISAM',\n";
	output += ");\n";
	output += "\\$driver_options = array (\n";
	output += ");\n";
	output += "\n";
	output += "\\$lastInstallTime = {lastInstallTime};\n";
	output += "\n";
	output += "\\$site_id = '{site_id}';\n";
	output += "\\$site_sessionname = '{site_sessionname}';\n";
	output += "\\$https_port = '443';\n";
	output += "\\$uuid = '{site_uuid}';\n";
	output += "\n";
	output += "if (!defined('MODX_CORE_PATH')) {\n";
	output += "    \\$modx_core_path= '{base_path}/core/';\n";
	output += "    define('MODX_CORE_PATH', \\$modx_core_path);\n";
	output += "}\n";
	output += "if (!defined('MODX_PROCESSORS_PATH')) {\n";
	output += "    \\$modx_processors_path= '{base_path}/core/model/modx/processors/';\n";
	output += "    define('MODX_PROCESSORS_PATH', \\$modx_processors_path);\n";
	output += "}\n";
	output += "if (!defined('MODX_CONNECTORS_PATH')) {\n";
	output += "    \\$modx_connectors_path= '{base_path}/connectors/';\n";
	output += "    \\$modx_connectors_url= '/connectors/';\n";
	output += "    define('MODX_CONNECTORS_PATH', \\$modx_connectors_path);\n";
	output += "    define('MODX_CONNECTORS_URL', \\$modx_connectors_url);\n";
	output += "}\n";
	output += "if (!defined('MODX_MANAGER_PATH')) {\n";
	output += "    \\$modx_manager_path= '{base_path}/manager/';\n";
	output += "    \\$modx_manager_url= '/manager/';\n";
	output += "    define('MODX_MANAGER_PATH', \\$modx_manager_path);\n";
	output += "    define('MODX_MANAGER_URL', \\$modx_manager_url);\n";
	output += "}\n";
	output += "if (!defined('MODX_BASE_PATH')) {\n";
	output += "    \\$modx_base_path= '{base_path}/';\n";
	output += "    \\$modx_base_url= '/';\n";
	output += "    define('MODX_BASE_PATH', \\$modx_base_path);\n";
	output += "    define('MODX_BASE_URL', \\$modx_base_url);\n";
	output += "}\n";
	output += "if(defined('PHP_SAPI') && (PHP_SAPI == 'cli' || PHP_SAPI == 'embed')) {\n";
	output += "    \\$isSecureRequest = false;\n";
	output += "} else {\n";
	output += "    \\$isSecureRequest = ((isset (\\$_SERVER['HTTPS']) && strtolower(\\$_SERVER['HTTPS']) == 'on') || \\$_SERVER['SERVER_PORT'] == \\$https_port);\n";
	output += "}\n";
	output += "if (!defined('MODX_URL_SCHEME')) {\n";
	output += "    \\$url_scheme=  \\$isSecureRequest ? 'https://' : 'http://';\n";
	output += "    define('MODX_URL_SCHEME', \\$url_scheme);\n";
	output += "}\n";
	output += "if (!defined('MODX_HTTP_HOST')) {\n";
	output += "    if(defined('PHP_SAPI') && (PHP_SAPI == 'cli' || PHP_SAPI == 'embed')) {\n";
	output += "        \\$http_host='{domain}';\n";
	output += "        define('MODX_HTTP_HOST', \\$http_host);\n";
	output += "    } else {\n";
	output += "        \\$http_host= array_key_exists('HTTP_HOST', \\$_SERVER) ? htmlspecialchars(\\$_SERVER['HTTP_HOST'], ENT_QUOTES) : '{domain}';\n";
	output += "        if (\\$_SERVER['SERVER_PORT'] != 80) {\n";
	output += "                    \\$http_host= str_replace(':' . \\$_SERVER['SERVER_PORT'], '', \\$http_host); // remove port from HTTP_HOST\n";
	output += "        }\n";
	output += "        \\$http_host .= (\\$_SERVER['SERVER_PORT'] == 80 || \\$isSecureRequest) ? '' : ':' . \\$_SERVER['SERVER_PORT'];\n";
	output += "        define('MODX_HTTP_HOST', \\$http_host);\n";
	output += "    }\n";
	output += "}\n";
	output += "if (!defined('MODX_SITE_URL')) {\n";
	output += "    \\$site_url= \\$url_scheme . \\$http_host . MODX_BASE_URL;\n";
	output += "    define('MODX_SITE_URL', \\$site_url);\n";
	output += "}\n";
	output += "if (!defined('MODX_ASSETS_PATH')) {\n";
	output += "    \\$modx_assets_path= '{base_path}/assets/';\n";
	output += "    \\$modx_assets_url= '/assets/';\n";
	output += "    define('MODX_ASSETS_PATH', \\$modx_assets_path);\n";
	output += "    define('MODX_ASSETS_URL', \\$modx_assets_url);\n";
	output += "}\n";
	output += "if (!defined('MODX_LOG_LEVEL_FATAL')) {\n";
	output += "    define('MODX_LOG_LEVEL_FATAL', 0);\n";
	output += "    define('MODX_LOG_LEVEL_ERROR', 1);\n";
	output += "    define('MODX_LOG_LEVEL_WARN', 2);\n";
	output += "    define('MODX_LOG_LEVEL_INFO', 3);\n";
	output += "    define('MODX_LOG_LEVEL_DEBUG', 4);\n";
	output += "}\n";
	output += "if (!defined('MODX_CACHE_DISABLED')) {\n";
	output += "    \\$modx_cache_disabled= false;\n";
	output += "    define('MODX_CACHE_DISABLED', \\$modx_cache_disabled);\n";
	output += "}\n";
	var ret = helpers.replaceTerms(output, config);
	return ret;
}

// prepareFor remote then it must be importing the local database into remote database
// if prepareFor local then it must be the importing the remote database into the local
recipe.mysqlProcessDumpFile = function(dumpFile, prepareFor) {
	var domainRemote = this.config.domain;
	var siteUrl = this.config.site_url;
	var domainLocal = api.getConfigFor('local', recipe).domain;
	if (prepareFor == 'remote') {
		// going from local to remote
		recipe.local.log('Copy and adapt the local database for import into the remote:' + this.environment + ' database');
		recipe.dbAdapt(domainLocal, domainRemote, dumpFile);
	} else {
		var siteUrl = domainLocal;
		recipe.local.log('Copy and adapt the remote:' + this.environment + ' database for import into the local database');
		// going from remote to local
		recipe.dbAdapt(domainRemote, domainLocal, dumpFile);
	}
};

/**
 * Adapt a modx database sql dump by replacing the old_url (url strings from the target db)
 * with new_url the url for the destination db. e.g. modx.com to modx.local
 * @param old_url string the string to search form
 * @param new_url string the string to replace the search string with
 * @param file file path to the db dump sql file
 * @return void
 */
recipe.dbAdapt = function(old_url, new_url, file) {
	recipe.local.log('Replace DB Urls, find: "' + old_url + '", replace with: "' + new_url + '"');
	var content = fs.readFileSync(file, {
		encoding: 'utf8'
	});
	var output = this.replaceUrls(old_url, new_url, content);
	fs.writeFileSync(file, output);
};

/**
 * Replace Url strings in the content
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param content string the string to search
 * @see this.replaceUrlsInString();
 * @see this.replaceUrlsInSerialized()
 * @source grunt-wordpress-deploy
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrls = function(search, replace, content) {
	content = this.replaceUrlsInSerialized(search, replace, content);
	content = this.replaceUrlsInString(search, replace, content);
	return content;
};

/**
 * Replace Url strings in php serialized strings
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param string string the string to search
 * @source grunt-wordpress-deploy
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrlsInSerialized = function(search, replace, string) {
	var length_delta = search.length - replace.length;
	var search_regexp = new RegExp(search, 'g');
	// Replace for serialized data
	var matches, length, delimiter, old_serialized_data, target_string, new_url, occurences;
	var regexp = /s:(\d+):([\\]*['"])(.*?)\2;/g;
	while (matches = regexp.exec(string)) {
		old_serialized_data = matches[0];
		target_string = matches[3];
		// If the string contains the url make the substitution
		if (target_string.indexOf(search) !== -1) {
			occurences = target_string.match(search_regexp).length;
			length = matches[1];
			delimiter = matches[2];
			// Replace the url
			new_url = target_string.replace(search_regexp, replace);
			length -= length_delta * occurences;
			// Compose the new serialized data
			var new_serialized_data = 's:' + length + ':' + delimiter + new_url + delimiter + ';';
			// Replace the new serialized data into the dump
			string = string.replace(old_serialized_data, new_serialized_data);
		}
	}
	return string;
};

/**
 * Replace Url strings in a string
 * @param search string the string to search form
 * @param replace string the string to replace the search string with
 * @param string string the string to search
 * @return string return the string with the matched searches replaced.
 */
recipe.replaceUrlsInString = function(search, replace, string) {
	var regexp = new RegExp('(?!' + replace + ')(' + search + ')', 'g');
	return string.replace(regexp, replace);
};

/**
 * Update the database with specific mysql queries based on the environment
 * @param  site_url the 
 * @return {[type]}          [description]
 */
recipe.updateDbFields = function(env) {
	var environment = env == 'remote' ? recipe.environment : env;
	var config = api.getConfigFor(environment, recipe);
	var site_url = config.site_url;

	var srv = env == 'local' ? recipe.local : recipe.remote;
	var pwd = env == 'local' ? '' : '-p{db.password}';

	// Replace the site_url in database config
	var cmd = "mysql -u{db.username} " + pwd + " '{db.database}' -e\
				\"UPDATE modx_clientconfig_setting SET value='"+site_url+"' WHERE modx_clientconfig_setting.key='site_url'\"";
	cmd = helpers.replaceTerms(cmd, config);
	srv.exec(cmd);

	// var query = "\"UPDATE modx_clientconfig_setting SET value='"+site_url+"' WHERE modx_clientconfig_setting.key='site_url'\"";
	// recipe.remote.exec("mysql -u{db.username} -p{db.password} {db.database} -e "+query);
};


module.exports = recipe;