/**
 * Created by nicoleneo on 09/05/2017.
 */
var _ = require('lodash');

/**
 * Finds values in the array that match a given regex
 * @param string regex to match on
 * @returns {Array.<*>} containing values that match the regex
 */
function findMatchingRegex(tableList, regex) {
	return tableList.filter(function(val) {return val.match(regex)});
}
/**
 * Expands a list of values by expanding the wildcard values with matching ones from allTables
 * If allTables is empty, the wildcard items won't be expanded, but the non-wildcard items will be retained in the new
 * list
 * @param allTables list of tables the wildcard values will be matched to
 * @returns Array list of values with the regex expanded
 */
function expandListWithMatchingTables(tableList, allTables) {
	var expandedList = tableList.reduce(function (acc, val, index, arr) {
		if (val.includes('*')) {
			// if the entry contains a wildcard, expand it
			var matchingTables = findMatchingRegex(allTables, val);
			// add the matching tables array to the acc
			acc = acc.concat(matchingTables);
		}
		else {
			// otherwise just add it to the acc
			acc = acc.concat(val);
		}
		return acc;
	}, []);
	return expandedList;
}

function pulldb(target) {
    console.log('nido pulldb '+ target.name);
    var excludeList = expandListWithMatchingTables(target.excludePull, allTables);
    var includeList = expandListWithMatchingTables(target.includePull, allTables);
    excludePull = _.uniq(_.difference(excludeList, includeList));
    var tablesToPull = _.uniq(_.difference(allTables, excludePull));
    console.log('Exclude pull: ', excludePull);
    console.log('List of tables to pull: ', tablesToPull);
    console.log("\n--------\n\n")
}

function pushdb(target) {
    console.log('nido pushdb '+ target.name);
    var excludeList = expandListWithMatchingTables(target.excludePush, allTables);
    var includeList = expandListWithMatchingTables(target.includePush, allTables);
    excludePush = _.uniq(_.difference(excludeList, includeList));
    var tablesToPush = _.uniq(_.difference(allTables, excludePush));
    console.log('Exclude push: ', excludePush);
    console.log('List of tables to push: ', tablesToPush);
}

 var a = ['apple', 'blueberry', 'plum', 'rambutan', 'strawberry', 'apple', 'jackfruit', 'longan'];
 var b = ['durian', 'apple', 'mango', 'longan', 'lychee'];


 console.log("A: ", a);
 console.log("B: ", b);

 console.log("unique A: ", _.uniq(a));
 console.log("A union B unique: ", _.uniq(_.union(a, b)));
 console.log("A intersect B unique: ", _.uniq(_.intersection(a, b)));

var allTables = ['wp_commentmeta', 'wp_comments', 'wp_links', 'wp_options', 'wp_postmeta', 'wp_posts', 'wp_rg_form', 'wp_rg_form_meta', 'wp_rg_form_view', 'wp_rg_lead', 'wp_rg_lead_detail', 'wp_rg_lead_detail_long', 'wp_rg_lead_meta', 'wp_rg_lead_note', 'wp_term_relationships', 'wp_term_taxonomy', 'wp_termmeta', 'wp_terms', 'wp_usermeta', 'wp_users', 'wp_wfBadLeechers', 'wp_wfBlockedIPLog', 'wp_wfBlocks', 'wp_wfBlocksAdv', 'wp_wfConfig', 'wp_wfCrawlers', 'wp_wfFileMods', 'wp_wfHits', 'wp_wfHoover', 'wp_wfIssues', 'wp_wfLeechers', 'wp_wfLockedOut', 'wp_wfLocs', 'wp_wfLogins', 'wp_wfNet404s', 'wp_wfReverseCache', 'wp_wfScanners', 'wp_wfStatus', 'wp_wfThrottleLog', 'wp_wfVulnScanners'];

var production = {
    name: 'production',
    excludePull: ['wp_wf*'],
    includePull: ['wp_wfConfig'],
    excludePush: ['wp_rg_*'],
    includePush: []
};

var staging = {
    name: 'staging',
    excludePull: ['wp_wf*'],
    includePull: ['wp_wfConfig'],
    excludePush: ['wp_wf*', 'wp_rg_*'],
    includePush: ['wp_rg_*']
};

pulldb(production);
pushdb(staging);