var request = require('request');
var Configstore = require('configstore');
var Fiber = require('fibers');
var promptly = require('promptly');
var Shell = require('./cmd/shell');
var errors = require('./cmd/errors');
var logger = require('./cmd/logger')();
var Ssh = require('./cmd/ssh');
var prettyTime = require('pretty-hrtime');
var extend = require('util-extend');
var helpers = require('./helpers');
var axios = require('axios');

// set up global variables:
var newiconConf = new Configstore('newicon', {'hub_authorization': null});
var hubAuthorisation = newiconConf.get('hub_authorization');
var hubDomain = 'https://hub.newicon.net';

// private variables
var _config = null;
var _envConfig = {};
var _repo = null;
var _releaseDir = null;

var api = {
	
	login: function() {
		console.log('Please enter your Newicon hub login credentials');
		promptly.prompt('email: ', function (err, email) {
			if (err) throw err;
			api.passwordPrompt(0, email);
		});
	},
	
	/**
	 * Prompts the hub for an access token (using login above) and stores it using Configstore via newiconConf here
	 *    ~/.config/configstore/newicon.json
	 */
	passwordPrompt: function(tries, email) {
		tries || 0; tries++;
		var options = {url: hubDomain + '/hosting/api/gettoken/'};
		// err is always null in this case, because no validators are set
		var callback = function(error, response, body) {
			if (response.statusCode !== 200) {
				if (tries !== 5) {
					// try again:
					console.log('attempt '+tries+' of 5: ', body);
					api.passwordPrompt(tries, email);
				} else {
					console.log('Oops, your account has no doubt been locked now as you have entered the incorrect password 5 times!');
				}
			} else {
				var body = JSON.parse(body);
				console.log('Success!');
				newiconConf.set('hub_authorization', body.authorization);
			}
		};
		promptly.password('password: ', function (err, password) {
			options.form = {email:email, password:password};
			request.post(options, callback);
		});
	},
	
	/**
	 * Logout and destroy the local token authenticating with the hub
	 */
	logout: function() {
		newiconConf.set('hub_authorization', null);
	},
	
	/**
	 * Whether the current user running nido is authorised
	 */
	isAuthorised: function() {
		return hubAuthorisation !== null;
	},
	
	/**
	 * Checks if authorised and throws an error if not
	 */
	ensureAuthorised: function(){
		if (! api.isAuthorised()) {
			var shell = new Shell;
			logger.error('Before you can run commands, you need to login with the Newicon hub.');
			shell.abort('Run: "nido login" then supply your Newicon hub credentials.');
		}
	},
	
	/**
	 * Gets the git repo for the project within the current directory
	 */
	findGitRepo: function() {
		if (_repo === null) {
			var shell = api.getShell();
			_repo = shell.exec('git config --get remote.origin.url', {failsafe:true}).stdout;
			if (_repo == null) {
				shell.abort('No git repo can be found here');
			}
			_repo = _repo.trim();
		}
		return _repo;
	},
	
	/**
	 * Store a server info objects keyed by servername
	 * This prevents multiple calls to the hub API during one session.
	 */
	serverInfoSessionCache: {},

	/**
	 * Get the server information (credentials) for the supplied server name
	 * @param {string} serverName
	 * @returns {unresolved}
	 */
	getServerData: function(serverName){
		if (api.serverInfoSessionCache[serverName] == undefined) {
			var options = {
				url: hubDomain + '/hosting/api/server/id/'+serverName,
				headers: {
					'authorization':'Basic ' + hubAuthorisation
				}
			};
			var fiber = Fiber.current;
			request(options, function(error, response, body){
				if (response.statusCode !== 200) {
					logger.error('failed', body);
				} else {
					var body = JSON.parse(body);
				}
				fiber.run(body);
			});
			api.serverInfoSessionCache[serverName] = Fiber.yield();
		}
		
		return api.serverInfoSessionCache[serverName];
	},

	getHubAuth: function() {
		return hubAuthorisation;
	},

	/**
	 * Load account records from the hub
	 * @param {Object} query - a search query
	 * @return Array - array of matched accounts objects
	 */
	getAccountData: function(query) {
		var fiber = Fiber.current;
		axios.post('https://hub.newicon.net/hosting/api/account', query, {
			timeout: 2000,
			headers: {'authorization': 'Basic ' + hubAuthorisation}
		}).then(response => {
			fiber.run(response.data);
		}).catch(error=>{
			throw new Error(error);
		});
		return Fiber.yield();
	},
	
	/**
	 * return the config like:
	 * 
	 * ```javascript
	 * {
	 *     "repo" : " **the git repo** ",
	 *     // environment objects
	 *     "environments": {
	 *         "test" : {
	 *             "domain": "my-project.com",
	 *             "server": "test.vm.newicon.net"
	 *         }
	 *     }
	 * }
	 * ```
	 * @alias getProjectConfig
	 * @param {string} environment
	 * @returns {api.getConfig.config}
	 */
	// getConfig: function() {
	// 	// use the repo as a key to look up project settings
	// 	var repo = api.findGitRepo();
	// 	// talk to the hub to get project settings.
	// 	// e.g. production: newicon.vm.newicon.net
	// 	if (_config === null) {
	// 		var deployConfig = {};//api.requireDeployConfigFile();
	// 		deployConfig.repo = repo;
	// 		deployConfig.releaseDir = api.getLatestReleaseFolderName();
	// 		return deployConfig;
	// 	}
	// 	return _config;
	// },
	
	/**
	 * NOTE : this merges the globally cached config so it may not do what you expect!!
	 *
	 * Returns the specific config for an environment
	 * It will return all config for the environments property
	 * E.g.
	 * ```javascript
	 * {
	 *     "domain": "my-project.com",
	 *     "server": "test.vm.newicon.net",
	 *     "gitBranch" : "develop", // optional if not defined it will use the master branch
	 *     "releaseDir" : "2015-xx-xx", // set automatically
	 *     "deployPath": "/srv/mywebsite.com", // optional, if deployPath is not defined, it will be /srv/{domain}
	 * }
	 * ```
	 * @param {string} environment
	 * @returns {api@call;getConfig.edeployPathnvironments}
	 */
	getConfigFor: function(environment, Recipe) {
		if (!environment) {
			api.abort('You must specify an environment');
		}
		if (! _envConfig[environment]) {
			var envConfig = {};
			envConfig = extend(envConfig, Recipe.config);
			envConfig = extend(envConfig, Recipe.config.environments[environment]);
			// set deployPath if not configured
			if (! envConfig.deployPath) {
				if (environment == 'local') {
					envConfig.deployPath = '~/Sites/' + envConfig.domain.replace('www.', '');
				} else {
					envConfig.deployPath = '/srv/' + envConfig.domain.replace('www.', '');
				}
			}
			
			// set releasePath
			// local is a special environment
			if (environment == 'local') {
				// the local environment has no concept of release paths
				envConfig.releasePath = envConfig.deployPath;	
			} else {
				envConfig.releasePath = envConfig.deployPath + '/releases/' + envConfig.releaseDir;	
			}
			
			// database config, should be looked up
			if (!envConfig.db) {
				console.error('The environment should specifiy db configuration');
			}
			envConfig.db.host = envConfig.db.host ? envConfig.db.host : 'localhost';
			delete envConfig.environments;
			_envConfig[environment] = envConfig;
		}
		return JSON.parse(JSON.stringify(_envConfig[environment]));
	},
	
	/**
	 * Encapsulate a series of shell commands in as Fiber.
	 * Also shows a nice timer for how long the task took.
	 * useage:
	 * 
	 * ```javascript
	 * api.task('Run my deployment task' function(){
	 *     shell.exec('...'); 
	 *     // ...
	 *     shell.exec('...');
	 * })
	 * ```
	 * This will then output the initial parameter as a nice log message.
	 * And, once complete it will display a message like "Finished after 133ms" 
	 * Note that this encapsulates the function in a Fiber making everything run sequentially inside.
	 * 
	 * @param string description
	 * @param function fn to be called in the context of a Fiber.run
	 * @returns {undefined}
	 */
	task: function(description, fn, options) {
		options = options || {};
		Fiber(function() {
			api.ensureAuthorised();
			if (!options.silent)
				logger.info('Running: ' + description);
			var t = process.hrtime();
			fn();
			if (!options.silent)
				logger.info('Finished after ' + prettyTime(process.hrtime(t)));
			if (api._ssh) {
				api._ssh.close();
			}
			if (api._sshRoot) {
				api._sshRoot.close();
			}
		}).run();
	},
	
	
	getSsh: function(config) {
		if (api._ssh === null) {
			api._ssh = new Ssh(config);
		}
		return api._ssh;
	},
	
	_ssh:null,
	_sshRoot:null,
	_shell:null,
	
	getShell: function() {
		if (api._shell === null) {
			api._shell = new Shell({remote:{host:'localhost'}});
		}
		return api._shell;
	},
	
	/**
	 * Looks up the server via the hub to attain the connection details
	 * Then returns a ssh object connected to that server
	 * @param {String} server the name of the server, it's ip address or domain identifier
	 * @returns {Ssh}
	 */
	getSshToServer: function(server) {
		var connectionInfo = api.getServerData(server);
		return api.getSsh({
			remote:{host:server},
			host: connectionInfo.server_host,
			username: connectionInfo.admin_username,
			password: connectionInfo.password_admin
		});
	},

	/**
	 * Get a root ssh connection
	 * @param {String} server
	 * @returns {Ssh}
	 */
	getSshRoot: function (server) {
		var connectionInfo = api.getServerData(server);
		if (api._sshRoot === null) {
			api._sshRoot = new Ssh({
				remote: {host:server},
				host: connectionInfo.server_host,
				username: 'root',
				password: connectionInfo.password_root
			});
		}
		return api._sshRoot;
	},

	/**
	 * Get the root mysql password for this server.
	 * @param {String} server the name of the server, it's ip address or domain identifyer
	 * @returns {String}
	 */
	getRootMysqlPassword: function(server) {
		var info = api.getServerData(server);
		return info.password_mysql_root;
	},
	
	/**
	 * returns a connected ssh object to the server specified in the environment
	 * for example:
	 * ```javascript
	 * // with a test environment config of environment: {test:{server:'test.vm.newicon.net'}}
	 * var ssh = api.getSshForEnvironment('test'); // returns a connection to test.vm.newicon.net
	 * ```
	 */
	getSshForEnvironment: function(environment) {
		var env = api.getConfigFor(environment);
		return api.getSshToServer(env.server);
	},
	
	/**
	 * Create a release folder string
	 * @return string a date formated folder name e.g. 2014-11-21-102910
	 */
	getLatestReleaseFolderName: function ()	{
		if (_releaseDir === null) {
			date = new Date();
			_releaseDir = date.getFullYear() + '-' +
			("0" + (date.getMonth() + 1)).slice(-2) + '-' +
			("0" + date.getDate()).slice(-2) + '-' +
			("0" + date.getHours()).slice(-2) +
			("0" + date.getMinutes()).slice(-2) +
			("0" + date.getSeconds()).slice(-2);
		}
		return _releaseDir;
	},
	
	/**
	 * @throw error
	 */
	abort: function(message) {
		throw new errors.AbortedError(message || 'Deployment aborted');
	},
	
	/**
	 * Factory method to build a recipe object
	 * Load a recipe object
	 * @param {string} environment
	 * @returns {Recipe}
	 */
	loadRecipe: function(environment) {
		var recipeObj = api.loadNidoFile();
		var recipe = recipeObj(api.loadNidoRecipe, environment);
		recipe.setup(environment);
		return recipe;
	},
	
	/**
	 * Loads a recipe object from the nido recipes/ directory
	 * e.g. common, wordpress, magento, etc
	 * @param {string} recipe name
	 * @throws error if no recipe is found
	 * @return {Object} recipe object
	 */
	loadNidoRecipe: function(recipeName){
		var recipePath = './recipes/' + recipeName;
		try {
			if (recipeName) {
				var recipeObj = require(recipePath);
			} else {
				var recipeObj = require('./recipes/common');
			}
		} catch (e) {
			if (e.code == 'MODULE_NOT_FOUND') {
				console.error('No recipe named "'+recipeName+'" exists.');
				throw e;
			} else {
				throw e;
			}
		}
		return recipeObj;
	},
	
	/**
	 * Loads the local project nido file
	 * @return ./recipe recipe object
	 */
	loadNidoFile: function() {
		var recipeObj = require(process.cwd() + '/nido.js');
		return recipeObj;
	}
	
};

module.exports = api;
