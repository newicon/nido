
var helpers = {

	/**
	 * Replace variables in a string that match the key names of the
	 * passed in javascript object
	 * ```
	 * msg = replaceTerms("I like {food}", {food:"BACON!"});
	 * "I like BACON!"
	 * ```
	 * Automatically flattens objects so nested objects can be used via . syntax
	 * for example:
	 *
	 * ```javascript
	 * helpers.replaceTerms('message: {universe.world.hello}', {universe:{world:{hello:'hello world!'}}})
	 * // gives 'message: hello world!'
	 * ```
	 *
	 * @string string the string containing variables to replace e.g. "hello {first_name} {last_name}"
	 * @terms object e.g. {first_name:"steve", last_name:"OBrien"}
	 * @return string with {*} elements replaced with their corresponding values in the terms object
	 * @see String.prototype.replaceTerms
	 */
	replaceTerms: function (string, terms) {
		// loop through the termsArray
		terms = helpers.flattenObject(terms);
		for (var term in terms) {
			var replace = new RegExp("\{" + term + "\}", "g");
			string = string.replace(replace, terms[term]);
		}
		return string;
	},

	/**
	 * similar to replaceTerms above but you can additionally use a '|' to specify a default when the term is not found
	 * - if no '|' is specified the results are exactly the same as for replaceTerms
	 * - all chars after the first '|' before the '}' are considered the default value
	 *
	 * ```javascript
	 * helpers.replaceTerms('missing:{universe.world.notfound|default} present:{universe.world.hello|missing}', {universe:{world:{hello:'hello world!'}}})
	 * // gives 'missing:default present:hello world!'
	 * ```
	 */
	replaceTermsWithDefaultsSupported: function(string, terms)
	{
		let flatTerms = helpers.flattenObject(terms);
		// loop through term matches
		var exp = new RegExp("{(.*?)}","g");
		var matches = string.match(exp);
		for (let idx in matches)
		{
			let match = matches[idx];
			let findTerm = match.substring(1, match.length-1);
			let pos = findTerm.indexOf('|');
			if (pos!==-1) {
				let defaultValue = findTerm.substring(pos+1);
				findTerm = findTerm.substring(0,pos);
				let replaceWithValue = (findTerm in flatTerms) ? flatTerms[findTerm] : defaultValue;
				string = helpers.replaceAllExact(string, match, replaceWithValue);
			}
			else {
				if (findTerm in flatTerms) {
					string = helpers.replaceAllExact(string, match, flatTerms[findTerm]);
				}
			}
		}
		return string;
	},

	/**
	 * Replace all exact (i.e. not regex) instances of a substring within a string by a value
	 * ```javascript
	 * helpers.replaceAllExact("this is isn't is something dumb", "is", "*)
	 * // gives "th* * *n't * something dumb"
	 * ```
	 */
	replaceAllExact(string, replace, replaceWith)
	{
		// surely there is a better way to do this?
		return string.split(replace).join(replaceWith);
	},

	/**
	 * Escape a string for use in console. For example:
	 *
	 * ```javascript
	 * exec('echo "' + helpers.escape(myStringToEscape) + '"')
	 * ```
	 * Currently escapes characters "!" "\" "/" "`" with a backslash "\!" "\\" "\/" "\`"
	 * @param {string} str
	 * @returns {string}
	 */
	escape: function (str) {
		return str.replace(/!|\|\\|`/gi, function myFunction(x) {
			return '\\' + x;
		});
	},

	/**
	 * Flatten an object
	 * @return object
	 * ```javascript
	 * var a = {
	 *     b : 'b'	
	 * }
	 * helpers.flattenObject(a);
	 * // returns: 'a.b':'b'
	 * ```
	 */
	flattenObject: function (ob) {
		var toReturn = {};
		for (var i in ob) {
			if (!ob.hasOwnProperty(i))
				continue;
			if ((typeof ob[i]) == 'object') {
				var flatObject = helpers.flattenObject(ob[i]);
				for (var x in flatObject) {
					if (!flatObject.hasOwnProperty(x))
						continue;
					toReturn[i + '.' + x] = flatObject[x];
				}
			} else {
				toReturn[i] = ob[i];
			}
		}
		return toReturn;
	},
	/**
	 * Finds values in the array that match a given regex
	 * @param string regex to match on
	 * @returns {Array.<*>} containing values that match the regex
	 */
	findMatchingRegex: function(tableList, regex) {
		return tableList.filter(function(val) {return val.match(regex)});
	},
	/**
	 * Expands a list of values by expanding the wildcard values with matching ones from allTables
	 * If allTables is empty, the wildcard items won't be expanded, but the non-wildcard items will be retained in the new
	 * list
	 * @param allTables list of tables the wildcard values will be matched to
	 * @returns Array list of values with the regex expanded
	 */
	expandListWithMatchingTables: function(tableList, allTables) {
		var that = this;
		var expandedList = tableList.reduce(function (acc, val, index, arr) {
			if (val.includes('*')) {
				// if the entry contains a wildcard, expand it
				var matchingTables = that.findMatchingRegex(allTables, val);
				// add the matching tables array to the acc
				acc = acc.concat(matchingTables);
			}
			else {
				// otherwise just add it to the acc
				acc = acc.concat(val);
			}
			return acc;
		}, []);
		return expandedList;
	}
};

module.exports = helpers;
